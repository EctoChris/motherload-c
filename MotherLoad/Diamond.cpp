#include "Diamond.hpp"

void Diamond::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Diamond::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = { xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 1, 1};
    
    textTexture = TextureManager::CreateTextTexture("+ 1 Diamond", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Diamond::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
