#ifndef Amazonite_hpp
#define Amazonite_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Amazonite: public Substance {
public:
    int value = 50000;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Amazonite(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Amazonite",  50000){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Amazonite();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};
#endif /* Amazonite_hpp */
