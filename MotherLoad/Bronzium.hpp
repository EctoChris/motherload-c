#ifndef Bronzium_hpp
#define Bronzium_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Bronzium: public Substance {
public:
    int value = 100;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect;
    SDL_Rect destRect;
    Bronzium(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Bronzium", 100){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Bronzium();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};
#endif /* Bronzium_hpp */
