#ifndef VeryRareItem_hpp
#define VeryRareItem_hpp

#include <stdio.h>
#include "Game.hpp"
#include "UltraRareItem.hpp"

using namespace std;

class VeryRareItem: public UltraRareItem{
public:
    VeryRareItem(int type) : UltraRareItem(type)
    {
        
    }
};
#endif /* VeryRareItem_hpp */
