#include "MineralProcessor.hpp"

MineralProcessor::MineralProcessor(Player* player, Game* game){
    this->player = player;
    this->game = game;
    successMessage = TextureManager::LoadTexture("SuccessMessage.png");
    mainScreen = TextureManager::LoadTexture("MineralProcessor.png");

}

MineralProcessor::~MineralProcessor(){
    SDL_DestroyTexture(successMessage);
    SDL_DestroyTexture(mainScreen);
}

void MineralProcessor::generateShopScreen(){
    
    SDL_Rect sourceRect = { 10, 10, 10, 10 };
    SDL_Rect destRect = { 50, 40, 800, 600 };
    TextureManager::DrawStill(mainScreen, sourceRect, destRect);
   // TextureManager::Draw(mainScreen, sourceRect, destRect);

    list<Substance*> ironiumList = list<Substance*>();
    list<Substance*> bronziumList = list<Substance*>();
    list<Substance*> goldiumList = list<Substance*>();
    list<Substance*> platoniumList = list<Substance*>();
    list<Substance*> emeraldList = list<Substance*>();
    list<Substance*> einsteiniumList = list<Substance*>();
    list<Substance*> rubyList = list<Substance*>();
    list<Substance*> diamondList = list<Substance*>();
    list<Substance*> amazoniteList = list<Substance*>();
    list<Substance*> saphireList = list<Substance*>();
    
    list<list<Substance*>> allSubstanceLists;
    bool addedIronium, addedBronzium, addedGoldium, addedPlatonium, addedEmerald, addedEinsteinium, addedRuby, addedDiamond, addedAmazonite, addedSaphire;
    addedIronium = addedBronzium = addedGoldium = addedPlatonium = addedEmerald = addedEinsteinium = addedRuby = addedSaphire = addedDiamond = addedAmazonite = false;
    for(const auto& Substance : player->inventory)
    {
       if(Substance->substanceType == "Ironium")
       {
           ironiumList.push_back(Substance);
           addedIronium = true;
       }
       else if (Substance->substanceType == "Bronzium")
       {
           bronziumList.push_back(Substance);
           addedBronzium = true;
       }
       else if (Substance->substanceType == "Goldium")
       {
           goldiumList.push_back(Substance);
           addedGoldium = true;
       }
       else if (Substance->substanceType == "Platonium")
       {
           platoniumList.push_back(Substance);
           addedPlatonium = true;
       }
       else if (Substance->substanceType == "Emerald")
       {
           emeraldList.push_back(Substance);
           addedEmerald = true;
       }
       else if(Substance->substanceType == "Einsteinium")
       {
           einsteiniumList.push_back(Substance);
           addedEinsteinium = true;
       }
       else if (Substance->substanceType == "Ruby")
       {
           rubyList.push_back(Substance);
           addedRuby = true;
       }
       else if (Substance->substanceType == "Diamond")
       {
           diamondList.push_back(Substance);
           addedDiamond = true;
       }
       else if (Substance->substanceType == "Amazonite")
       {
            amazoniteList.push_back(Substance);
            addedAmazonite = true;
       }
       else if (Substance->substanceType == "Saphire")
       {
           saphireList.push_back(Substance);
           addedSaphire = true;
       }
    }
    
    if(addedIronium)
        allSubstanceLists.push_back(ironiumList);
    
    if(addedGoldium)
        allSubstanceLists.push_back(goldiumList);
    
    if(addedBronzium)
        allSubstanceLists.push_back(bronziumList);
    
    if(addedPlatonium)
        allSubstanceLists.push_back(platoniumList);
    
    if(addedEmerald)
        allSubstanceLists.push_back(emeraldList);
    
    if(addedEinsteinium)
        allSubstanceLists.push_back(einsteiniumList);
    
    if(addedRuby)
        allSubstanceLists.push_back(rubyList);
    
    if(addedDiamond)
        allSubstanceLists.push_back(diamondList);
    
    if(addedAmazonite)
        allSubstanceLists.push_back(amazoniteList);
    
    if(addedSaphire)
        allSubstanceLists.push_back(saphireList);
    
    
    int rowNum = 0;
    int rowXPos = 145;
    int rowYPos = 155;
    //int nameColXPos = rowXPos + 40;

    destRect = { rowXPos, rowYPos, 40, 40 };
    int overallTotal = 0;
    for(const auto& list : allSubstanceLists)
    {
        //Draw Substance:
        destRect = { rowXPos, rowYPos, 40, 40 };
        Substance* tempSubstance = list.front();
        TextureManager::DrawStill(tempSubstance->texture, sourceRect, destRect);
      //  TextureManager::Draw(tempSubstance->texture, sourceRect, destRect);
        
        //Amount of Substance:
        destRect = { rowXPos + 80, rowYPos+10, 100, 20 };
        string amount = to_string(list.size());
        string times = "   x   ";
        string totalString = amount.append(times);
        const char* amountCharacter = totalString.c_str();
        TextureManager::DrawStill(TextureManager::CreateTextTexture(amountCharacter, Game::greenColor), sourceRect, destRect);
       // TextureManager::Draw(TextureManager::CreateTextTexture(amountCharacter, Game::greenColor), sourceRect, destRect);
        
        
        //Substance Type:
        destRect = {rowXPos + 220, rowYPos+10, 100, 20 };
        TextureManager::DrawStill(TextureManager::CreateTextTexture(tempSubstance->substanceType, Game::greenColor), sourceRect, destRect);
        
        //TotalCost of Substance:
        destRect = {rowXPos + 450, rowYPos+10, 60, 20 };
        int totalCost = tempSubstance->value * list.size();
        overallTotal += totalCost;
        string totalCostString = to_string(totalCost);
        string dollarSign = "$ ";
        string totalCostLabelString = dollarSign.append(totalCostString);
        const char* totalCostLabel = totalCostLabelString.c_str();
        TextureManager::DrawStill(TextureManager::CreateTextTexture(totalCostLabel, Game::greenColor), sourceRect, destRect);
        
        cout<<overallTotal<<endl;
        rowYPos += 45;
    }
    
    totalSaleValue = overallTotal;
    
    //Draw Total:
    destRect = {rowXPos + 450, 565, 140, 20 };
    string overallTotalString = to_string(overallTotal);
    string total = "Total: ";
    string totalSaleString = total.append(overallTotalString);
    const char* totalSaleLabel = totalSaleString.c_str();
    TextureManager::DrawStill(TextureManager::CreateTextTexture(totalSaleLabel, Game::greenColor), sourceRect, destRect);
    
    if(!addedGoldium && !addedIronium && !addedBronzium)
        stockToSell = false;
    else
        stockToSell = true;
}

void MineralProcessor::showSuccessMessage(){
    
    //Drawing GUI Canvas:
    SDL_Rect destRect = { 200, 200, 500, 200 };
    SDL_Rect sourceRect = { 10, 10, 10, 10 };
    //TextureManager::Draw(successMessage, sourceRect, destRect);
    TextureManager::DrawStill(successMessage, sourceRect, destRect);
    
    //Drawing Text Message:
    destRect = { 230, 240, 400, 50 };
    string overalTotalString = to_string(lastSale);
    string message = "Thanks for doing business, here is your $";
    string thankYouMessage = message.append(overalTotalString);
    const char* thankYouMessageChar = thankYouMessage.c_str();
    TextureManager::DrawStill(TextureManager::CreateTextTexture(thankYouMessageChar, Game::greenColor), sourceRect, destRect);
}

void MineralProcessor::handleInputs(){
    
}

void MineralProcessor::sellAll(){
    lastSale=totalSaleValue;

    player->money += totalSaleValue;
    player->inventory.clear();
}
