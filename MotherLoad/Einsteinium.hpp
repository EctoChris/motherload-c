#ifndef Einsteinium_hpp
#define Einsteinium_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Einsteinium: public Substance {
public:
    int value = 1000;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Einsteinium(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) :Substance(xPos, yPos, texture, destRect, "Einsteinium", 1000)
    {
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Einsteinium();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};

#endif /* Einsteinium_hpp */


