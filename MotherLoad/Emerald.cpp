#include "Emerald.hpp"

void Emerald::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Emerald::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = { xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 1, 1};
    
    textTexture = TextureManager::CreateTextTexture("+ 1 Emerald", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Emerald::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
