#ifndef Player_hpp
#define Player_hpp

#include "Animation.hpp"
#include "Map.hpp"

using namespace std;

class Player {
public:
    int xPos;
    int yPos;
    int playerHealth = 100;
    int money = 0;
    int drillSkillLevel = 1;
    int engineSkillLevel = 1;
    int fuelTankSkillLevel = 1;
    //int drillSpeed = 10;
    int drillSpeed = 1;
    int engineSpeed = 2;
    double fuelCost = 0.08; //0.05
    double fuel = 100;
    double gravity = 2;
    int playerScore = 0;
    static list<Substance*> inventory;
    bool currentlyDrilling = false;
    bool onMineralProcessorIsland = false;
    bool onFuelStationIsland = false;
    bool onUpgradeShopIsland = false;
    bool onHealthStation = false;
    bool playerIsDead = false;
    int currentDrillingDirection;
    SDL_Texture* objTexture;
    SDL_Texture* heart;
    SDL_Texture* whiteHeart;
    SDL_Rect sourceRect, destRect;
    SDL_Rect* collisionBox;
    Substance* substanceBeingDrilled = nullptr;
    Substance* lastSubstanceBeingDrilled = nullptr;
    Mix_Chunk* foundSubstanceSound;
    int currentAnimation, lastDirectionFaced;
    int vehicleSize;
    int distanceDrilled = 0;
    SDL_Point velocity;
    Player(int startXPos, int startYPos, double vehicleSize, Mix_Chunk* substanceSound);
    ~Player();
    void Render();
    void update(float DT);
    void draw();
    void move(int action);
    void updateCollisionBox();
    void updatePlayerStats();
    void changeAnimation(int newState, bool resetFrameToBeginning);//Abstract function
    bool checkCollisionBelow(int action);
    bool checkCollisionLeft();
    bool checkCollisionRight();
    bool checkCollisionAbove();
    bool checkCollisionWithMapBoundsLeft();
    bool checkCollisionWithMapBoundsRight();
    bool checkPlayerOnNonSubstanceSurface();
    bool checkIfNearAlien();
    bool checkIfCanAffordEndGame();
    bool checkForPlayerDeath();
    void changeAnimation(int animation);
    void drillInDirection(int direction);
    bool isSubstanceBelowPlayer(Substance* s);
    bool isSubstanceLeftOfPlayer(Substance* s);
    bool isSubstanceRightOfPlayer(Substance* s);
    bool isSubstanceAbovePlayer(Substance* s);
    Substance* getSubstanceDirectlyBelow();
    Substance* getSubstanceToLeft();
    Substance* getSubstanceToRight();
    void drillIntoSubstance(Substance* s, int direction);
    void destroySubstance();
    void removeSubstanceFromMap();
    void updateMovement(float dt);
    void drawFuelTank();
    void drawPlayerHealth();
    void checkForFallDamage();
    void damageFlash();
    void playFoundSubstanceSound();
};

#endif /* Player_hpp */
