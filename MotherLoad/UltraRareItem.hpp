#ifndef UltraRareItem_hpp
#define UltraRareItem_hpp

#include <stdio.h>
#include "Game.hpp"

using namespace std;

class UltraRareItem {
public:
    string name;
    UltraRareItem(int type);
};
#endif /* UltraRareItem_hpp */
