#include "Ruby.hpp"

void Ruby::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Ruby::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = { xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 1, 1};
    
    textTexture = TextureManager::CreateTextTexture("+ 1 Ruby", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Ruby::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
