#ifndef CameraManager_hpp
#define CameraManager_hpp

#include <stdio.h>
#include "Player.hpp"

using namespace std;

class CameraManager {
public:
    Player* player;
    void update();
    
};
#endif /* CameraManager_hpp */
