#include "UpgradeShop.hpp"


enum itemSelection {
    drill = 1,
    engine = 2,
    fuel = 3
};

enum screenSelection {
    titleScreen = 0,
    gameScreen = 1,
    mineralProcessorScreen = 2,
    fuelStationScreen = 3,
    upgradeShopScreen = 4,
    alienSpeechFail = 5,
    alienSpeechSuccess = 6,
    endGameAnimation = 7
};


UpgradeShop::UpgradeShop(Player* p, Game* g){
    this->player = p;
    this->game = g;
    
    //Load all items:
    commonDrill = TextureManager::LoadTexture("greyDrill.png");
    commonEngine = TextureManager::LoadTexture("greyEngine.png");
    commonFuelTank = TextureManager::LoadTexture("greyFuelTank.png");
    
    rareDrill = TextureManager::LoadTexture("greenDrill.png");
    rareEngine = TextureManager::LoadTexture("greenEngine.png");
    rareFuelTank = TextureManager::LoadTexture("greenFuelTank.png");
    
    veryRareDrill = TextureManager::LoadTexture("blueDrill.png");
    veryRareEngine = TextureManager::LoadTexture("blueEngine.png");
    veryRareFuelTank = TextureManager::LoadTexture("blueFuelTank.png");
    
    ultraRareDrill = TextureManager::LoadTexture("goldDrill.png");
    ultraRareEngine = TextureManager::LoadTexture("goldEngine.png");
    ultraRareFuelTank = TextureManager::LoadTexture("goldFuelTank.png");
    
    successMessage = TextureManager::LoadTexture("SuccessMessage.png");
    mainScreen = TextureManager::LoadTexture("newUpgradeShop.png");

    currentPlayerItem = nullptr;
    nextAvailablePlayerItem = nullptr;
}

UpgradeShop::~UpgradeShop(){
    SDL_DestroyTexture(currentPlayerItem);
    SDL_DestroyTexture(nextAvailablePlayerItem);
    SDL_DestroyTexture(commonDrill);
    SDL_DestroyTexture(rareDrill);
    SDL_DestroyTexture(veryRareDrill);
    SDL_DestroyTexture(ultraRareDrill);
    SDL_DestroyTexture(commonEngine);
    SDL_DestroyTexture(rareEngine);
    SDL_DestroyTexture(veryRareEngine);
    SDL_DestroyTexture(ultraRareEngine);
    SDL_DestroyTexture(commonFuelTank);
    SDL_DestroyTexture(rareFuelTank);
    SDL_DestroyTexture(veryRareFuelTank);
    SDL_DestroyTexture(ultraRareFuelTank);
    SDL_DestroyTexture(successMessage);
    SDL_DestroyTexture(mainScreen);
    
}

void UpgradeShop::showSuccessMessage(){
    //Drawing GUI Canvas:
    SDL_Rect destRect = { 200, 200, 500, 200 };
    SDL_Rect sourceRect = { 10, 10, 10, 10 };
    //TextureManager::Draw(successMessage, sourceRect, destRect);
    TextureManager::DrawStill(successMessage, sourceRect, destRect);
    string itemName = "null";
    if(currentItemFocus == drill)
        itemName = "drill";
    else if(currentItemFocus == engine)
        itemName = "engine";
    else if (currentItemFocus == fuel)
        itemName = "fuel tank";

    //Drawing Text Message:
    destRect = { 230, 240, 400, 50 };
    if(lastUpgradeWasSuccess)
    {
        string lastSaleCostString = to_string(lastSaleCost);
        string message = "You upgraded your ";
        string message2 = " for $";
        string totalMessage = message.append(itemName).append(message2).append(lastSaleCostString);
        const char* totalMessageChar = totalMessage.c_str();
        TextureManager::DrawStill(TextureManager::CreateTextTexture(totalMessageChar, Game::greenColor), sourceRect, destRect);
    }
    else {
        string sorryMessage = "Sorry you cannot afford this upgrade";
        const char* sorryMessageChar =sorryMessage.c_str();
        TextureManager::DrawStill(TextureManager::CreateTextTexture(sorryMessageChar, Game::greenColor), sourceRect, destRect);
    }
}

void UpgradeShop::generateShopScreen(){
    currentItemValue = 250;
    nextUpgradeCost = 1000;
    
    SDL_Rect sourceRect = { 10, 10, 10, 10 };
    SDL_Rect destRect = { 50, 40, 800, 600 };
    TextureManager::DrawStill(mainScreen, sourceRect, destRect);
    
    string confirmUpgrade = "CONFIRM UPGRADE";
    destRect =  { 320, 555, 175, 30 };
    const char* confirmUpgradeChar = confirmUpgrade.c_str();
    TextureManager::DrawStill(TextureManager::CreateTextTexture(confirmUpgradeChar, Game::greenColor), sourceRect, destRect);
    
    string currentString = "CURRENT:";
    const char* currentChar = currentString.c_str();
    destRect = { 100, 310, 90, 30 };
    TextureManager::DrawStill(TextureManager::CreateTextTexture(currentChar, Game::greenColor), sourceRect, destRect);
    
    string availableUpgradeString = "AVAILABLE UPGRADE:";
    const char* availableUpgradeChar =availableUpgradeString.c_str();
    destRect = { 450, 310, 170, 30 };
    TextureManager::DrawStill(TextureManager::CreateTextTexture(availableUpgradeChar, Game::greenColor), sourceRect, destRect);
    
    switch(currentItemFocus)
    {
        case drill:{
            itemName = "DRILL";
            itemDescription = "Upgrading your drill increases the speed at which you can mine";
            if(player->drillSkillLevel == 1)
            {
                updateShop( 250, 1000, "Common Drill", "Rare Drill", commonDrill, rareDrill);
            }
            else if (player->drillSkillLevel == 2)
            {
                updateShop(1000, 5000, "Rare Drill", "Very Rare Drill", rareDrill, veryRareDrill);
            }
            else if (player->drillSkillLevel == 3)
            {
                updateShop(5000, 30000, "Very Rare Drill", "Ultra Rare Drill", veryRareDrill, ultraRareDrill);
            }
            else if (player->drillSkillLevel == 4)
            {
                updateShop(30000, NULL, "Ultra Rare Drill", "", ultraRareDrill, nullptr);
            }
            break;
        }
        case engine: {
            itemName = "ENGINE";
            itemDescription = "Upgrading your engine increases the speed at which your vehicle can drive";
            if(player->engineSkillLevel == 1)
            {
                updateShop(250, 1000, "Common Engine", "Rare Engine", commonEngine, rareEngine);
            }
            else if (player->engineSkillLevel == 2)
            {
                updateShop(1000, 5000, "Rare Engine", "Very Rare Engine", rareEngine, veryRareEngine);
            }
            else if (player->engineSkillLevel == 3)
            {
                updateShop(5000, 30000, "Very Rare Engine", "Ultra Rare Engine", veryRareEngine, ultraRareEngine);
            }
            else if (player->engineSkillLevel == 4)
            {
                updateShop(30000, NULL, "Ultra Rare Engine", "", ultraRareEngine, nullptr);
            }
            break;
        }
        case fuel: {
            itemName = "FUEL";
            itemDescription = "Upgrade your fuel tank to be able improve your vehicles fuel efficiency";
            if(player->fuelTankSkillLevel == 1)
            {
                updateShop(250, 1000, "Common FuelTank", "Rare FuelTank", commonFuelTank, rareFuelTank);
            }
            else if (player->fuelTankSkillLevel == 2)
            {
                updateShop(1000, 5000, "Rare FuelTank", "Very Rare FuelTank", rareFuelTank, veryRareFuelTank);
            }
            else if (player->fuelTankSkillLevel == 3)
            {
                updateShop(5000, 30000, "Very Rare FuelTank", "Ultra Rare FuelTank", veryRareFuelTank, ultraRareFuelTank);
            }
            else if (player->fuelTankSkillLevel == 4)
            {
                updateShop(30000, NULL, "Ultra Rare FuelTank", "", ultraRareFuelTank, nullptr);
            }
            break;
        }
        default:
            break;
    }
    
    //Drawing Item Name:
    const char* itemNameChar = itemName.c_str();
    destRect = { 180, 80, 130, 30 };
    TextureManager::DrawStill(TextureManager::CreateTextTexture(itemNameChar, Game::greenColor), sourceRect, destRect);
    
    //Drawing Item Description:
    const char* itemDescriptionChar = itemDescription.c_str();
    destRect = { 120, 250, 670, 30 };
    TextureManager::DrawStill(TextureManager::CreateTextTexture(itemDescriptionChar, Game::greenColor), sourceRect, destRect);

    //Drawing Current Item
    destRect = { 100, 345, 180, 180 };
    TextureManager::DrawStill(currentPlayerItem, sourceRect, destRect);
    
    //Drawing Current Item Full Name:
    int lettersCount = currentItemFullName.length();
    const char* name = currentItemFullName.c_str();
    destRect = {290, 345, lettersCount * 10, 30 };
    TextureManager::DrawStill(TextureManager::CreateTextTexture(name, Game::greenColor), sourceRect, destRect);

    //Drawing Current Item Value:
    string currentItemValueString = to_string(currentItemValue);
    lettersCount = currentItemValueString.length();
    string dollarSign = "$";
    string currentItemValueMessage = dollarSign.append(currentItemValueString);
    const char* currentItemValueChar = currentItemValueMessage.c_str();
    destRect = {290, 390, lettersCount * 15, 30 };
    TextureManager::DrawStill(TextureManager::CreateTextTexture(currentItemValueChar, Game::greenColor), sourceRect, destRect);

    //Drawing Next Available Upgrade:
    destRect = { 450, 345, 180, 180 };
    TextureManager::DrawStill(nextAvailablePlayerItem, sourceRect, destRect);
    
    //Drawing Next Upgrade Item Full Name:
    if(nextUpgradeItemFullName != "")
    {
        lettersCount = nextUpgradeItemFullName.length();
        name = nextUpgradeItemFullName.c_str();
        destRect = {640, 345, lettersCount * 10, 30 };
        TextureManager::DrawStill(TextureManager::CreateTextTexture(name, Game::greenColor), sourceRect, destRect);
    }
    
    //Drawing Next Upgrade Cost:
    if(nextUpgradeCost != NULL)
    {
        string nextItemCostString = to_string(nextUpgradeCost);
        lettersCount = nextItemCostString.length();
        dollarSign = "$";
        string nextItemCostMessage = dollarSign.append(nextItemCostString);
        const char* nextItemCostChar = nextItemCostMessage.c_str();
        destRect = {640, 390, lettersCount * 15, 30 };
        TextureManager::DrawStill(TextureManager::CreateTextTexture(nextItemCostChar, Game::greenColor), sourceRect, destRect);
    }
}

void UpgradeShop::changeItemFocus(int selection){
    
    
}

void UpgradeShop::handleInputs(const Uint8* keyStates){
    
    if(keyStates[SDL_SCANCODE_1])
        currentItemFocus = drill;
    
    if(keyStates[SDL_SCANCODE_2])
        currentItemFocus = engine;
    
    if(keyStates[SDL_SCANCODE_3])
        currentItemFocus = fuel;
    
    if(keyStates[SDL_SCANCODE_RETURN])
    {
        SDL_Delay(100);
        if(showingSuccessScreen)
        {
            showingSuccessScreen = false;
            Game::currentScreen = gameScreen;
        }
        else
        {
            switch(currentItemFocus)
            {
                case drill:{
                    if(player->drillSkillLevel < 4)
                    {
                        if(player->money >= nextUpgradeCost)
                        {
                            player->money -= nextUpgradeCost;
                            player->drillSkillLevel++;
                            lastSaleCost = nextUpgradeCost;
                            lastUpgradeWasSuccess = true;
                            showingSuccessScreen = true;
                            player->updatePlayerStats();
                        }
                        else
                        {
                            lastUpgradeWasSuccess = false;
                            showingSuccessScreen = true;
                        }
                    }
                    break;
                }
                case engine: {
                    if(player->engineSkillLevel < 4)
                    {
                        if(player->money >= nextUpgradeCost)
                        {
                            player->money -= nextUpgradeCost;
                            player->engineSkillLevel++;
                            lastSaleCost = nextUpgradeCost;
                            lastUpgradeWasSuccess = true;
                            showingSuccessScreen = true;
                            player->updatePlayerStats();
                        }
                        else
                        {
                            lastUpgradeWasSuccess = false;
                            showingSuccessScreen = true;
                        }
                    }
                    break;
                }
                case fuel: {
                    if(player->fuelTankSkillLevel < 4)
                    {
                        if(player->money >= nextUpgradeCost)
                        {
                            player->money -= nextUpgradeCost;
                            player->fuelTankSkillLevel++;
                            lastSaleCost = nextUpgradeCost;
                            lastUpgradeWasSuccess = true;
                            showingSuccessScreen = true;
                            player->updatePlayerStats();
                        }
                        else
                        {
                            lastUpgradeWasSuccess = false;
                            showingSuccessScreen = true;
                        }
                    }
                    break;
                }
            }
        }
    }
}

void UpgradeShop::updateShop(int currentItemValue, int nextUpgradeCost, string currentItemFullName, string nextUpgradeItemName, SDL_Texture* currentPlayerItem, SDL_Texture* nextAvailableUpgrade){
    
    this->currentItemValue = currentItemValue;
    this->nextUpgradeCost = nextUpgradeCost;
    this->currentItemFullName = currentItemFullName;
    this->nextUpgradeItemFullName = nextUpgradeItemName;
    this->currentPlayerItem = currentPlayerItem;
    this->nextAvailablePlayerItem = nextAvailableUpgrade;
}
