#include "Bronzium.hpp"

void Bronzium::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}


void Bronzium::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = {xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 100, 200};
    textTexture = TextureManager::CreateTextTexture("+ 1 Bronzium", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Bronzium::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;

    }
}
