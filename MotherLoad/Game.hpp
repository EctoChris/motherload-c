#ifndef Game_hpp
#define Game_hpp

#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>
#include "TextureManager.hpp"
#include <list>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

using namespace std;

class Game {
public:
    static TTF_Font* caviarFont;
    static SDL_Color greenColor;
    static SDL_Color blackColor;
    static SDL_Color redColor;
    static int currentScreen, screenWidth, screenHeight;
    Game();
    ~Game();
    void init (const char* title, int xpos, int ypos, int width, int height, bool fullscreen);
    void initText();
    void endGameAnimation();
    void update();
    void render();
    void clean();
    bool running() {return isRunning; }
    static SDL_Renderer* renderer;
    static SDL_Rect camera;
    static SDL_Event event;
    //The dimensions of the level
    static int LEVEL_WIDTH;
    static int LEVEL_HEIGHT;
    bool isRunning;
    
private:
    int gameCounter = 0;
    SDL_Window *window;
    SDL_Texture* spriteTexture;
//    GameObject* player;
};


#endif /* Game_hpp */
