#include "Player.hpp"
#include "MineralProcessor.hpp"
#include "FuelStation.hpp"
#include <string>
#include "CameraManager.hpp"
#include "UpgradeShop.hpp"
#include "HealthShop.hpp"
#include <fstream>

using namespace std;

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Surface *windowSurface = NULL;
SDL_Surface *imageSurface = NULL;
Map *map = nullptr;
Game *game = nullptr;
CameraManager cameraManager;
Player *player = NULL;
SDL_Texture *shopTexture;
SDL_Texture *fuelStationTexture;
SDL_Texture *alienMessageFail;
SDL_Texture *alienMessageSuccess;
SDL_Texture *gameWinMessage;
SDL_Texture *gameLossMessage;
SDL_Texture *money;
SDL_Texture *gameScoreMessage = nullptr;
SDL_Texture *titleMenuCanvas = nullptr;
SDL_Texture *instructionsCanvas = nullptr;
SDL_Texture *backToMenuTex = nullptr;
SDL_Texture *playAgainTex = nullptr;
SDL_Texture *highScoreResult = nullptr;
MineralProcessor* mineralProcessor = nullptr;
FuelStation* fuelStation = nullptr;
UpgradeShop* upgradeShop = nullptr;
HealthShop* healthStation = nullptr;
int previousHighScore;
double fuelCost = 0.2;
bool clickEnabled = true;
bool newHighScoreWritten = false;

const double vehicleSize = 0.10;

enum itemSelection {
    drill = 1,
    engine = 2,
    fuel = 3
};

enum screenSelection {
    titleScreen = 0,
    gameScreen = 1,
    mineralProcessorScreen = 2,
    fuelStationScreen = 3,
    upgradeShopScreen = 4,
    alienSpeechFail = 5,
    alienSpeechSuccess = 6,
    endGameAnimation = 7,
    healthStationMessage = 8,
    instructionsScreen = 9
};

enum animation {
    idle = 0,
    driveRight = 1,
    driveLeft = 2,
    drillRight = 3,
    drillLeft = 4,
    drillDown = 5,
    flyUp = 6,
    flyRight = 7,
    flyLeft = 8,
    idleLeft = 9,
    flyUpLeft = 10,
    drillDownLeft = 11,
    explosion = 12
};

enum channel {
    EXPLOSIONCHANNEL = 0,
    DRILLINGCHANNEL = 1,
    HELICOPTERCHANNEL = 2,
    DRIVINGCHANNEL = 3,
    SUCCESSFULPURCHASECHANNEL = 4,
    MUSICCHANNEL = 5,
    DINGCHANNEL = 6
};

enum keyboardInput {
    UP = 0,
    DOWN = 1,
    LEFT = 2,
    RIGHT = 3,
    TOPRIGHT = 4,
    BOTTOMRIGHT = 5,
    BOTTOMLEFT = 6,
    TOPLEFT = 7,
    SPACE = 8,
    FALL = 9,
    DRILLDOWN = 10,
    DRILLRIGHT = 11,
    DRILLLEFT = 12,
    CONTINUEDRILLING = 13
};

//Method Prototypes:
void runHighScoreConditionsAndCreateTexture();
int getHighScore();
void writeNewHighScore(int playerHighScore);
void playSound(int channel, Mix_Chunk* sound);


int main(int argc, const char * argv[]) {
    
    //Initialization of game=============================================================================//
    game = new Game();
    if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        //old window dimensions = 800 x 640
        window = SDL_CreateWindow("MotherLoad", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Game::screenWidth, Game::screenHeight, false);
        Game::renderer = SDL_CreateRenderer(window, -1, 0);
        
        if(Game::renderer)
            SDL_SetRenderDrawColor(renderer, 255, 0, 15, 255);
        
        game->isRunning= true;
    }
    else {
        game->isRunning = false;
    }
    
    if( !(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
    {
        cout<<"Could not initialize SDL_Image library"<<endl;
    }
    
    if(TTF_Init() == -1) {
        cout<<"Could not initialize SDL_ttf library"<<endl;
    }
    
    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 6, 4096) == -1)
    {
        cout<<"Mixer could not load"<<endl;
        SDL_Quit();
    }
    else {
        cout<<"Yeww mixer loaded"<<endl;
    }
    game->initText();
    
    //setup camera size and position for first time use
    Game::camera = { 0, 0, Game::screenWidth, Game::screenHeight };
    
    //=============TEXT DRAWING===========================================================================//

    //Text destination:
    SDL_Rect textDestination = { 5, 5, 90, 30 };
    SDL_Rect sourceRect = { 100, 200};
    SDL_Rect moneyTextPos  { 390, 5, 40, 35 };

    //Drawing section ===================================================================================//
    //SDL_SetRenderDrawColor(Game::renderer, 135,206,250, 0);
    SDL_SetRenderDrawColor(Game::renderer, 200, 80, 50, 0);
    SDL_Rect* r = new SDL_Rect;
    r->x = 0;
    r->y = 0;
    r->w = 900;
    r->h = 840;
    Mix_Chunk *BANG = Mix_LoadWAV("bang.wav");
    Mix_Chunk *HelicopterSound = Mix_LoadWAV("HelicopterSound.wav");
    Mix_Chunk *badEngine = Mix_LoadWAV("BadEngine.wav");
    Mix_Chunk *sickEngine = Mix_LoadWAV("SickEngine.wav");
    Mix_Chunk *drillSound = Mix_LoadWAV("FinalDrill.wav");
    Mix_Chunk *successfulPurchaseSound = Mix_LoadWAV("ChaChing.wav");
    Mix_Chunk *music = Mix_LoadWAV("MotherLoadSoundtrack.wav");
    Mix_Chunk *ding = Mix_LoadWAV("Ding.wav");
    
    if (!BANG || !HelicopterSound || !badEngine || !sickEngine || !drillSound || !successfulPurchaseSound || !music || !ding)
        std::cerr << "Failed to load effect\n";
    
    SDL_RenderDrawRect(Game::renderer, r);
    SDL_RenderFillRect(Game::renderer, r);
    SDL_Rect titleSourceRect = { 0, 0, 10, 10 };
    SDL_Rect titleDestRect = { 0, 0, Game::screenWidth, Game::screenHeight };
    Animation vehicleMove = Animation("motherloadSpriteFinished.png", 4, 825, 750, 0.10, 255,255,255, true, 0, 0);
    Animation vehicleFly = Animation("motherloadSpriteFinished.png", 9, 825, 750, 0.02, 255,255,255, true, 0, 812);
    Animation vehicleDrillHorizontal = Animation("motherloadSpriteFinished.png", 4, 825, 750, 0.05, 255,255,255, true, 0, 1642);
    Animation vehicleDrillDown = Animation("motherloadSpriteFinished.png", 4, 825, 750, 0.05, 255,255,255, true, 0, 2470);
    Animation vehicleIdle = Animation("motherloadSpriteFinished.png", 4, 825, 750, 0.05, 255,255,255, true, 0, 3300);
    Animation endGameRocket = Animation("EndGameRocket.png", 6, 415, 490, 0.9, 255,255,255, true, 0, 0);
    Animation deathExplosion = Animation("FireExplosionSprite.png", 12, 83, 83, 0.04, 255,255,255, true, 0, 0);
    alienMessageFail = TextureManager::LoadTexture("AlienSpeechFail.png");
    alienMessageSuccess = TextureManager::LoadTexture("AlienSpeechSuccess.png");
    titleMenuCanvas = TextureManager::LoadTexture("motherLoadMenu.png");
    instructionsCanvas = TextureManager::LoadTexture("motherloadInstructions.png");
    gameWinMessage = TextureManager::CreateTextTexture("Game Complete!", Game::greenColor);
    gameLossMessage = TextureManager::CreateTextTexture("Game Over", Game::redColor);
    backToMenuTex = TextureManager::LoadTexture("BackToMenu.png");
    playAgainTex = TextureManager::LoadTexture("playAgain.png");
    
    bool windowOpen = true;
    SDL_RenderClear(Game::renderer);
    SDL_RenderPresent(Game::renderer);
    
    while(windowOpen)
    {
        switch(Game::currentScreen)
        {
            //Title Screen event handling + drawing
            case titleScreen: {
                SDL_RenderClear(Game::renderer);
                
                SDL_Delay(15);
                Mix_FadeOutChannel(MUSICCHANNEL, 500);
                const Uint8 *keyStates = SDL_GetKeyboardState(NULL);
                SDL_Event e;
                SDL_PollEvent(&e);
                if(keyStates[SDL_SCANCODE_ESCAPE] || e.type == SDL_QUIT)
                {
                    windowOpen = false;
                    SDL_Quit();
                }
                else if (keyStates[SDL_SCANCODE_RETURN])
                {
                    game->currentScreen = gameScreen;
                }
                else if (keyStates[SDL_SCANCODE_SPACE])
                {
                    game->currentScreen = instructionsScreen;
                }
                TextureManager::DrawStill(titleMenuCanvas, titleSourceRect, titleDestRect);
                
                SDL_RenderPresent(Game::renderer);
                break;
            }
            //Instructions Screen event handling + drawing
            case instructionsScreen: {
                SDL_RenderClear(Game::renderer);
                Mix_FadeOutChannel(MUSICCHANNEL, 500);
                //Handle Events:
                SDL_Event e;
                SDL_Delay(15);
                
                const Uint8 *keyStates = SDL_GetKeyboardState(NULL);
                SDL_PollEvent(&e);
                if(e.type == SDL_QUIT)
                {
                    windowOpen = false;
                    SDL_Quit();
                }
                
                if(keyStates[SDL_SCANCODE_ESCAPE])
                {
                    SDL_Delay(200);
                    Game::currentScreen = titleScreen;
                }
                
                TextureManager::DrawStill(instructionsCanvas, titleSourceRect, titleDestRect);
                SDL_RenderPresent(Game::renderer);
                break;
            }
            case gameScreen:{
                //Load last high score from txt file
                previousHighScore = getHighScore();
                newHighScoreWritten = false;
                highScoreResult = nullptr;
                //generate game objects
                map = new Map();
                player = new Player(500, 300, vehicleSize, ding);
                mineralProcessor = new MineralProcessor(player, game);
                fuelStation = new FuelStation(player, game);
                upgradeShop = new UpgradeShop(player, game);
                healthStation = new HealthShop(player, game);
                map->DrawMap();
                player->draw();
                game->isRunning = true;
                cameraManager.player = player;
                
                Uint32 lastUpdate = SDL_GetTicks(); //Setup lastUpdate
                Uint32 lastClick = SDL_GetTicks();
                
                while(game->isRunning == true)
                {
                    if(!Mix_Playing(MUSICCHANNEL))
                        playSound(MUSICCHANNEL, music);
                    //Draw Sky:
                    SDL_SetRenderDrawColor(Game::renderer, 135,206,250, 0);
                    
                    //difference is current time running minus the last update time
                    Uint32 timeDiff = SDL_GetTicks() - lastUpdate;
                    float DT = timeDiff / 1000.0;
                    lastUpdate = SDL_GetTicks();
                    SDL_Event e;
                    //Handle Events:
                    SDL_PollEvent(&e);
                    SDL_Delay(15);
                    
                    const Uint8 *keyStates = SDL_GetKeyboardState(NULL);
                    
                    if(e.type == SDL_QUIT)
                    {
                        windowOpen = false;
                        game->isRunning = false;
                        SDL_Quit();
                    }
                    
                    //Stop falling if player on substance / island / bottom of map
                    if(player->checkCollisionBelow(player->currentAnimation) || player->checkPlayerOnNonSubstanceSurface())
                    {
                        player->velocity.y = 0;
                    }
                    
                    //If player is currently drilling, continue running same animation and moving until a certain distance has been moved
                    if(player->currentlyDrilling)
                    {
                        player->move(CONTINUEDRILLING);
                        player->fuel -= player->fuelCost;
                    }
                    else
                    {
                        //UP:
                        if (keyStates[SDL_SCANCODE_UP])
                        {
                            if(player->checkCollisionAbove())
                                player->velocity.y = 0;
                            
                            //UP + RIGHT:
                            if(keyStates[SDL_SCANCODE_RIGHT])
                            {
                                player->currentAnimation = flyRight;
                                player->lastDirectionFaced = RIGHT;
                                
                                if(player->checkCollisionWithMapBoundsLeft() || player->checkCollisionLeft())
                                    player->velocity.x = fabs(player->velocity.x);
                                
                                if(player->checkCollisionRight() || player->checkCollisionWithMapBoundsRight())
                                {
                                    player->velocity.x = 0;
                                    if(!player->checkCollisionAbove())
                                    {
                                        player->move(UP);
                                    }
                                }
                                else if(player->checkCollisionAbove())
                                {
                                    if(!player->checkCollisionRight() || player->checkCollisionWithMapBoundsRight())
                                        player->move(RIGHT);
                                }
                                else
                                {
                                    player->move(TOPRIGHT);
                                }
                                
                            }
                            //UP + LEFT:
                            else if (keyStates[SDL_SCANCODE_LEFT])
                            {
                                player->currentAnimation = flyLeft;
                                player->lastDirectionFaced = LEFT;
                                
                                if (player->checkCollisionWithMapBoundsRight() || player->checkCollisionRight())
                                    player->velocity.x = -player->velocity.x;
                                
                                if(player->checkCollisionLeft() || player->checkCollisionWithMapBoundsLeft())
                                {
                                    player->velocity.x = 0;
                                    if(!player->checkCollisionAbove())
                                    {
                                        player->move(UP);
                                    }
                                }
                                else if (player->checkCollisionAbove())
                                {
                                    if(!player->checkCollisionLeft() || player->checkCollisionWithMapBoundsLeft())
                                    {
                                        player->move(LEFT);
                                    }
                                }
                                else
                                {
                                    player->move(TOPLEFT);
                                }
                            }
                            else {
                                if(player->lastDirectionFaced == LEFT)
                                {
                                    player->currentAnimation = flyUpLeft;
                                    if(player->checkCollisionLeft() || player->checkCollisionWithMapBoundsLeft())
                                        player->velocity.x = 0;
                                }
                                else
                                {
                                    player->currentAnimation = flyUp;
                                    if(player->checkCollisionRight() || player->checkCollisionWithMapBoundsRight())
                                        player->velocity.x = 0;
                                }
                                
                                if(!player->checkCollisionAbove())
                                {
                                    player->move(UP);
                                }
                            }
                            player->fuel -= player->fuelCost;
                        }
                        
                        //RIGHT:
                        if(keyStates[SDL_SCANCODE_RIGHT] && !keyStates[SDL_SCANCODE_UP] && !keyStates[SDL_SCANCODE_DOWN] && !keyStates[SDL_SCANCODE_LEFT])
                        {
                            if(player->checkCollisionBelow(player->currentAnimation) || player->checkPlayerOnNonSubstanceSurface())
                            {
                                if(!player->checkCollisionWithMapBoundsRight())
                                {
                                    player->currentAnimation = driveRight;
                                    player->move(RIGHT);
                                    player->lastDirectionFaced = RIGHT;
                                }
                                
                                if(player->checkCollisionRight())
                                {
                                    player->currentAnimation = drillRight;
                                    player->move(DRILLRIGHT);
                                }
                                player->fuel -= player->fuelCost;
                            }
                            player->move(FALL);
                        }
                        //LEFT:
                        if(keyStates[SDL_SCANCODE_LEFT] && !keyStates[SDL_SCANCODE_UP] && !keyStates[SDL_SCANCODE_DOWN] && !keyStates[SDL_SCANCODE_RIGHT])
                        {
                            if(player->checkCollisionBelow(player->currentAnimation) || player->checkPlayerOnNonSubstanceSurface())
                            {
                                if(!player->checkCollisionWithMapBoundsLeft())
                                {
                                    player->currentAnimation = driveLeft;
                                    player->move(LEFT);
                                    player->lastDirectionFaced = LEFT;
                                }
                                
                                if(player->checkCollisionLeft())
                                {
                                    player->currentAnimation = drillLeft;
                                    player->move(DRILLLEFT);
                                }
                                player->fuel -= player->fuelCost;
                            }
                            player->move(FALL);
                        }
                        
                        //DOWN:
                        if(keyStates[SDL_SCANCODE_DOWN] && !keyStates[SDL_SCANCODE_UP] && !keyStates[SDL_SCANCODE_LEFT] && !keyStates[SDL_SCANCODE_RIGHT]){
                            if(player->checkPlayerOnNonSubstanceSurface())
                                continue;
                            
                            if(player->checkCollisionBelow(player->currentAnimation))
                            {
                                if(player->lastDirectionFaced == LEFT)
                                    player->currentAnimation = drillDownLeft;
                                else
                                    player->currentAnimation = drillDown;
                                
                                player->move(DRILLDOWN);
                                player->fuel -= player->fuelCost;
                            }
                            if(Game::currentScreen == gameScreen)
                            {
                                if(player->checkCollisionLeft() || player->checkCollisionRight() || player->checkCollisionWithMapBoundsRight() || player->checkCollisionWithMapBoundsLeft())
                                    player->velocity.x = 0;
                                player->move(FALL);
                            }
                        }
                        
                        //IDLE
                        if(!keyStates[SDL_SCANCODE_DOWN] && !keyStates[SDL_SCANCODE_LEFT] && !keyStates[SDL_SCANCODE_RIGHT] && !keyStates[SDL_SCANCODE_UP]){
                            if (player->lastDirectionFaced == LEFT)
                                player->currentAnimation = idleLeft;
                            else
                                player->currentAnimation = idle;
                            
                            if(Game::currentScreen == gameScreen)
                            {
                                if(player->checkCollisionLeft() || player->checkCollisionRight() || player->checkCollisionWithMapBoundsRight() || player->checkCollisionWithMapBoundsLeft())
                                    player->velocity.x = 0;
                                player->move(FALL);
                            }
                        }
                        
                        
                        //Space Bar handling of in-game interacting with shop screens + alien
                        if(keyStates[SDL_SCANCODE_SPACE])
                        {
                            
                            if(Game::currentScreen == alienSpeechFail)
                            {
                                SDL_Delay(200);
                                Game::currentScreen = gameScreen;
                            }
                            else if(Game::currentScreen == upgradeShopScreen)
                            {
                                SDL_Delay(200);
                                Game::currentScreen = gameScreen;
                            }
                            else if (Game::currentScreen == alienSpeechSuccess)
                            {
                                SDL_Delay(200);
                                Game::currentScreen = gameScreen;
                                game->isRunning = false;
                            }
                            else if(player->checkIfNearAlien())
                            {
                                if(player->checkIfCanAffordEndGame())
                                {
                                    playSound(SUCCESSFULPURCHASECHANNEL, successfulPurchaseSound);
                                    SDL_Delay(200);
                                    Game::currentScreen = alienSpeechSuccess;
                                }
                                else
                                {
                                    SDL_Delay(200);
                                    Game::currentScreen = alienSpeechFail;
                                }
                            }
                            else
                            {
                                if(player->onFuelStationIsland)
                                {
                                    if(Game::currentScreen == fuelStationScreen)
                                    {
                                        SDL_Delay(200);
                                        Game::currentScreen = gameScreen;
                                    }
                                    else if (Game::currentScreen == gameScreen)
                                    {
                                        SDL_Delay(200);
                                        Game::currentScreen = fuelStationScreen;
                                    }
                                }
                                else if (player->onMineralProcessorIsland)
                                {
                                    if(Game::currentScreen == mineralProcessorScreen)
                                    {
                                        if(mineralProcessor->showingSuccessMessage)
                                            mineralProcessor->showingSuccessMessage = false;
                                        
                                        SDL_Delay(200);
                                        Game::currentScreen = gameScreen;
                                    }
                                    else if(Game::currentScreen == gameScreen)
                                    {
                                        SDL_Delay(200);
                                        Game::currentScreen = mineralProcessorScreen;
                                    }
                                }
                                else if (player->onUpgradeShopIsland){
                                    SDL_Delay(110);
                                    if(Game::currentScreen == gameScreen)
                                        Game::currentScreen = upgradeShopScreen;
                                    else
                                        Game::currentScreen = gameScreen;
                                }
                                else if (player->onHealthStation)
                                {
                                    SDL_Delay(110);
                                    if(healthStation->showingSuccessMessage)
                                    {
                                        Game::currentScreen = gameScreen;
                                        healthStation->showingSuccessMessage = false;
                                    }
                                    else{
                                        if(healthStation->healPlayer())
                                        {
                                            Game::currentScreen = healthStationMessage;
                                            playSound(SUCCESSFULPURCHASECHANNEL, successfulPurchaseSound);
                                        }
                                    }
                                }
                            }
                        }
                        
                        //Return key for interacting with shops / accepting messages
                        if(keyStates[SDL_SCANCODE_RETURN])
                        {
                            if(Game::currentScreen == mineralProcessorScreen)
                            {
                                SDL_Delay(200);
                                if(!mineralProcessor->showingSuccessMessage && mineralProcessor->stockToSell)
                                {
                                    mineralProcessor->sellAll();
                                    playSound(SUCCESSFULPURCHASECHANNEL, successfulPurchaseSound);
                                    mineralProcessor->showingSuccessMessage = true;
                                }
                                else {
                                    mineralProcessor->showingSuccessMessage = false;
                                    Game::currentScreen = gameScreen;
                                }
                            }
                            else if (Game::currentScreen == fuelStationScreen)
                            {
                                SDL_Delay(200);

                                if(!fuelStation->showingSuccessMessage)
                                {
                                    fuelStation->fillTank();
                                    playSound(SUCCESSFULPURCHASECHANNEL, successfulPurchaseSound);
                                    fuelStation->showingSuccessMessage = true;
                                }
                                else {
                                    fuelStation->showingSuccessMessage = false;
                                    Game::currentScreen = gameScreen;
                                }
                            }
                        }
                        
                        // 'MAT cheatCode This cheat code will fully upgrade all upgradable items + give you max fuel + $1000,000 (enough for end game)
                        if(keyStates[SDL_SCANCODE_M] && keyStates[SDL_SCANCODE_A] && keyStates[SDL_SCANCODE_T])
                        {
                            SDL_Delay(200);
                            player->money += 1000000;
                            player->drillSkillLevel = 4;
                            player->engineSkillLevel = 4;
                            player->fuelTankSkillLevel = 4;
                            player->updatePlayerStats();
                            player->playerHealth = 100;
                            player->fuel = 100;
                        }
                        
                        if(Game::currentScreen == upgradeShopScreen)
                        {
                            upgradeShop->handleInputs(keyStates);
                        }
                    }
                    
                    if(Game::currentScreen == gameScreen)
                    {
                        player->updateMovement(DT);
                        
                        SDL_RenderClear(Game::renderer);
                        map->DrawAlien(DT);
                        map->UpdateMap(DT);
                        if(player->checkForPlayerDeath())
                            player->currentAnimation = explosion;
                        
                        SDL_Point center = { 10, 10 };
                        int xCalibration = 19;
                        int yCalibration = 4;
                        
                        //Run player's current animation:
                        switch(player->currentAnimation)
                        {
                            case idle:{
                                Mix_FadeOutChannel(HELICOPTERCHANNEL, 200);
                                Mix_FadeOutChannel(DRIVINGCHANNEL, 40);
                                Mix_FadeOutChannel(DRILLINGCHANNEL, 40);

                                vehicleIdle.update(DT);
                                vehicleIdle.draw((player->xPos -xCalibration) - Game::camera.x, (player->yPos - yCalibration) -Game::camera.y, vehicleSize);
                                break;
                            }
                            case idleLeft:{
                                Mix_FadeOutChannel(HELICOPTERCHANNEL, 200);
                                Mix_FadeOutChannel(DRIVINGCHANNEL, 40);
                                Mix_FadeOutChannel(DRILLINGCHANNEL, 40);


                                vehicleIdle.update(DT);
                                vehicleIdle.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize, true);
                                break;
                            }
                            case driveRight:{
                                Mix_FadeOutChannel(HELICOPTERCHANNEL, 200);
                                Mix_FadeOutChannel(DRILLINGCHANNEL, 40);

                                if(!Mix_Playing(DRIVINGCHANNEL))
                                {
                                    if(player->drillSkillLevel == 4)
                                        playSound(DRIVINGCHANNEL, sickEngine);
                                    else
                                        playSound(DRIVINGCHANNEL, badEngine);
                                }
                                
                                vehicleMove.update(DT);
                                vehicleMove.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize);
                                break;
                            }
                            case driveLeft:{
                                Mix_FadeOutChannel(HELICOPTERCHANNEL, 200);
                                Mix_FadeOutChannel(DRILLINGCHANNEL, 40);
                                if(!Mix_Playing(DRIVINGCHANNEL))
                                {
                                    if(player->drillSkillLevel == 4)
                                        playSound(DRIVINGCHANNEL, sickEngine);
                                    else
                                        playSound(DRIVINGCHANNEL, badEngine);
                                }
                                
                                vehicleMove.update(DT);
                                vehicleMove.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize, true);
                                break;
                            }
                                
                            case drillRight:{
                                if(!Mix_Playing(DRILLINGCHANNEL))
                                {
                                    playSound(DRILLINGCHANNEL, drillSound);
                                }
                                vehicleDrillHorizontal.update(DT);
                                vehicleDrillHorizontal.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize);
                                player->substanceBeingDrilled->ShowSuccessMessage(player->substanceBeingDrilled->collisionBox.x, player->substanceBeingDrilled->collisionBox.y, DT);
                                break;
                            }
                            case drillLeft:{
                                if(!Mix_Playing(DRILLINGCHANNEL))
                                {
                                    playSound(DRILLINGCHANNEL, drillSound);
                                }
                                vehicleDrillHorizontal.update(DT);
                                vehicleDrillHorizontal.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize, true);
                                player->substanceBeingDrilled->ShowSuccessMessage(player->substanceBeingDrilled->collisionBox.x, player->substanceBeingDrilled->collisionBox.y, DT);
                                break;
                            }
                            case drillDown:{
                                if(!Mix_Playing(DRILLINGCHANNEL))
                                {
                                    playSound(DRILLINGCHANNEL, drillSound);
                                }
                                Mix_FadeOutChannel(HELICOPTERCHANNEL, 200);
                                vehicleDrillDown.update(DT);
                                vehicleDrillDown.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize);
                                player->substanceBeingDrilled->ShowSuccessMessage(player->substanceBeingDrilled->collisionBox.x, player->substanceBeingDrilled->collisionBox.y, DT);
                                break;
                            }
                            case flyUp:{
                                Mix_FadeOutChannel(DRILLINGCHANNEL, 40);
                                Mix_FadeOutChannel(DRIVINGCHANNEL, 40);

                                if(!Mix_Playing(HELICOPTERCHANNEL))
                                    playSound(HELICOPTERCHANNEL, HelicopterSound);
                              
                                vehicleFly.update(DT);
                                vehicleFly.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize);
                                break;
                            }
                            case flyRight:{
                                Mix_FadeOutChannel(DRIVINGCHANNEL, 40);
                                Mix_FadeOutChannel(DRILLINGCHANNEL, 40);

                                if(!Mix_Playing(HELICOPTERCHANNEL))
                                    playSound(HELICOPTERCHANNEL, HelicopterSound);
                                vehicleFly.update(DT);
                                vehicleFly.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize, 30, &center, false);
                                break;
                            }
                            case flyLeft: {
                                Mix_FadeOutChannel(DRIVINGCHANNEL, 40);
                                Mix_FadeOutChannel(DRILLINGCHANNEL, 40);

                                if(!Mix_Playing(HELICOPTERCHANNEL))
                                    playSound(HELICOPTERCHANNEL, HelicopterSound);
                                vehicleFly.update(DT);
                                vehicleFly.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize, 330, &center, true);
                                break;
                            }
                            case flyUpLeft:{
                                Mix_FadeOutChannel(DRIVINGCHANNEL, 40);
                                Mix_FadeOutChannel(DRILLINGCHANNEL, 40);

                                if(!Mix_Playing(HELICOPTERCHANNEL))
                                    playSound(HELICOPTERCHANNEL, HelicopterSound);
                                cout<<"FlyUpLeft"<<endl;
                                vehicleFly.update(DT);
                                vehicleFly.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize, true);
                                break;
                            }
                            case drillDownLeft: {
                                Mix_FadeOutChannel(HELICOPTERCHANNEL, 200);

                                if(!Mix_Playing(DRILLINGCHANNEL))
                                {
                                    playSound(DRILLINGCHANNEL, drillSound);
                                }
                                vehicleDrillDown.update(DT);
                                vehicleDrillDown.draw((player->xPos-xCalibration) - Game::camera.x, (player->yPos - yCalibration) - Game::camera.y, vehicleSize, true);
                                player->substanceBeingDrilled->ShowSuccessMessage(player->substanceBeingDrilled->collisionBox.x, player->substanceBeingDrilled->collisionBox.y, DT);
                                break;
                            }
                            case explosion: {
                                playSound(EXPLOSIONCHANNEL, BANG);
                                deathExplosion.update(DT);
                                deathExplosion.draw((player->xPos - 60) - Game::camera.x, (player->yPos - 60) - Game::camera.y, 2.0);
                                break;
                            }
                            default:{
                                break;
                            }
                        }
                        
                        //Drawing Player Score:
                        string playerScoreString = to_string(player->playerScore);
                        string scoreLabel = "Score: ";
                        string playerScoreLabel = scoreLabel.append(playerScoreString);
                        SDL_Texture* score = TextureManager::CreateTextTexture(playerScoreLabel.c_str(), Game::blackColor);
                        TextureManager::DrawStill(score, sourceRect, textDestination);
                        
                        //Drawing Player Money:
                        string playerMoney = to_string(player->money);
                        string dollarSign = "$";
                        int widthOfText = playerMoney.length() * 20;
                        string playerMoneyLabel = dollarSign.append(playerMoney);
                        
                        money = TextureManager::CreateTextTexture(playerMoneyLabel.c_str(), Game::blackColor);
                        moneyTextPos.w = widthOfText;
                        TextureManager::DrawStill(money, sourceRect, moneyTextPos);
                    }
                    
                    
                    //Drawing Player Fuel:
                    player->drawFuelTank();
                    
                    //Drawing Player Health:
                    player->drawPlayerHealth();
                    
                    if(player->lastSubstanceBeingDrilled != nullptr)
                        player->lastSubstanceBeingDrilled->updateMessage();
                    
                    player->checkForFallDamage();
                    
                    //Mineral Processing menu:
                    if(Game::currentScreen == mineralProcessorScreen)
                    {
                        mineralProcessor->generateShopScreen();
                        if(mineralProcessor->showingSuccessMessage)
                        {
                            mineralProcessor->showSuccessMessage();
                        }
                    } //FuelStation Menu:
                    else if (Game::currentScreen == fuelStationScreen)
                    {
                        fuelStation->generateShopScreen();
                        if(fuelStation->showingSuccessMessage)
                        {
                            fuelStation->showSuccessMessage(fuelStation->lastSaleWasSuccess);
                        }
                    } //Alien Speech Bubble:
                    else if(Game::currentScreen == alienSpeechSuccess)
                    {
                        SDL_Rect sourceRect = { 10, 10, 10, 10 };
                        SDL_Rect destRect = { 50, 40, 800, 600 };
                        TextureManager::DrawStill(alienMessageSuccess, sourceRect, destRect);
                    }
                    else if (Game::currentScreen == alienSpeechFail)
                    {
                        SDL_Rect sourceRect = { 10, 10, 10, 10 };
                        SDL_Rect destRect = { 50, 40, 800, 600 };
                        TextureManager::DrawStill(alienMessageFail, sourceRect, destRect);
                    }
                    else if (Game::currentScreen == upgradeShopScreen)
                    {
                        upgradeShop->generateShopScreen();
                        if(upgradeShop->showingSuccessScreen)
                        {
                            upgradeShop->showSuccessMessage();
                        }
                    }
                    else if (Game::currentScreen == healthStationMessage)
                    {
                        healthStation->generateSuccessMessage(healthStation->lastHealWasSuccess);
                    }
                    
                    SDL_RenderPresent(Game::renderer);
                    cameraManager.update();
                    if(player->playerIsDead)
                    {
                        game->isRunning = false;
                    }
                }
                
                //End game Animation:
                if(windowOpen)
                {
                    string playerScoreString = to_string(player->playerScore);
                    string message = "Your score = ";
                    string totalMessage = message.append(playerScoreString);
                    const char* totalMessageChar = totalMessage.c_str();
                    gameScoreMessage = TextureManager::CreateTextTexture(totalMessageChar, Game::blackColor);
                    
                    SDL_Rect sourceRectEnd = {10, 10};
                    SDL_Rect destRect = { 200, 150, 500, 200 };
                    SDL_Rect scoreDestRect = { 250, 350, 300, 100 };
                    SDL_Rect backToMenuDestRect = { 50, 550, 200, 80 };
                    SDL_Rect playAgainDestRect = { Game::screenWidth - 250, 555, 200, 80 };
                    SDL_Rect highScoreDestRect = {150, 430, 600, 100 };
                    bool endGameScreen = true;
                    if(!player->playerIsDead)
                    {
                        Uint32 startOfAnimation = SDL_GetTicks();
                        bool animationRunning = true;
                        bool timeToFly = false;
                        while(animationRunning)
                        {
                            //Draw Sky:
                            SDL_SetRenderDrawColor(Game::renderer, 135,206,250, 0);
                            Uint32 timeDiff = SDL_GetTicks() - lastUpdate;
                            Uint32 timeSinceStartOfAnimation = SDL_GetTicks() - startOfAnimation;
                            float timeToSwapDT = timeSinceStartOfAnimation / 1000.0;
                            float DT = timeDiff / 1000.0;
                            SDL_RenderClear(Game::renderer);
                            map->UpdateMap(DT);
                            map->DrawSpaceShipFlying(DT, timeToFly);
                            
                            if(timeToSwapDT < 2)
                            {
                                map->DrawAlienRunning(DT);
                            }
                            else {
                                timeToFly = true;
                                map->updateFlyingSpaceShip(DT);
                            }
                            if(timeToSwapDT > 8)
                            {
                                animationRunning = false;
                            }
                            
                            lastUpdate = SDL_GetTicks();
                            
                            SDL_RenderPresent(Game::renderer);
                            cameraManager.update();
                        }
                        while(!animationRunning)
                        {
                            //Handle Events:
                            SDL_Event e;
                            
                            SDL_Delay(15);
                            
                            const Uint8 *keyStates = SDL_GetKeyboardState(NULL);
                            SDL_PollEvent(&e);
                            
                            if(e.type == SDL_QUIT)
                            {
                                cout<<"Quit now"<<endl;
                                animationRunning = true;
                                windowOpen = false;
                            }
                            
                            if(keyStates[SDL_SCANCODE_ESCAPE])
                            {
                                SDL_Delay(200);
                                animationRunning = true;
                                map->~Map();
                                game->currentScreen = titleScreen;
                            }
                            else if (keyStates[SDL_SCANCODE_RETURN])
                            {
                                SDL_Delay(200);
                                map->~Map();
                                animationRunning = true;
                                game->currentScreen = gameScreen;
                            }
                            
                            //HighScoreConditions
                            runHighScoreConditionsAndCreateTexture();
                            
                            TextureManager::DrawStill(gameWinMessage, sourceRectEnd, destRect);
                            TextureManager::DrawStill(gameScoreMessage, sourceRectEnd, scoreDestRect);
                            TextureManager::DrawStill(backToMenuTex, sourceRectEnd, backToMenuDestRect);
                            TextureManager::DrawStill(playAgainTex, sourceRectEnd, playAgainDestRect);
                            TextureManager::DrawStill(highScoreResult, sourceRectEnd, highScoreDestRect);
                            
                            SDL_RenderPresent(Game::renderer);
                        }
                    } //player is Dead
                    else {
                        Uint32 startOfDeathExplosion = SDL_GetTicks();
                        bool timeToClearNow = false;
                        while(endGameScreen)
                        {
                            Uint32 timeDiff = SDL_GetTicks() - lastUpdate;
                            float DT = timeDiff / 1000.0;
                            lastUpdate = SDL_GetTicks();
                            Uint32 timeSinceStartOfDeathExplosion = SDL_GetTicks() - startOfDeathExplosion;
                            float timeToClearExplosion = timeSinceStartOfDeathExplosion / 1000.0;
                            
                            SDL_RenderClear(Game::renderer);
                            
                            map->UpdateMap(DT);
                            if(timeToClearExplosion < 0.67)
                            {
                                deathExplosion.runThroughAnimation(DT);
                                deathExplosion.draw((player->xPos - 60) - Game::camera.x, (player->yPos - 60) - Game::camera.y, 2.0);
                            }
                            
                            //Handle Events:
                            SDL_Event e;
                            SDL_PollEvent(&e);
                            
                            SDL_Delay(15);
                            
                            const Uint8 *keyStates = SDL_GetKeyboardState(NULL);
                            if(e.type == SDL_QUIT)
                            {
                                cout<<"Quit now"<<endl;
                                endGameScreen = false;
                                windowOpen = false;
                            }
                            if(keyStates[SDL_SCANCODE_ESCAPE])
                            {
                                SDL_Delay(200);
                                map->~Map();
                                endGameScreen = false;
                                game->currentScreen = titleScreen;
                            }
                            else if (keyStates[SDL_SCANCODE_RETURN])
                            {
                                SDL_Delay(200);
                                map->~Map();
                                player->inventory.clear();
                                endGameScreen = false;
                                game->currentScreen = gameScreen;
                            }
                            
                            //HighScore Conditions:
                            runHighScoreConditionsAndCreateTexture();
                            
                            TextureManager::DrawStill(highScoreResult, sourceRectEnd, highScoreDestRect);
                            TextureManager::DrawStill(gameLossMessage, sourceRectEnd, destRect);
                            TextureManager::DrawStill(gameScoreMessage, sourceRectEnd, scoreDestRect);
                            TextureManager::DrawStill(backToMenuTex, sourceRectEnd, backToMenuDestRect);
                            TextureManager::DrawStill(playAgainTex, sourceRectEnd, playAgainDestRect);
                            
                            SDL_RenderPresent(Game::renderer);
                        }
                    }
                }
                
                
                
                break;
            }
            default:
                break;
        }
    }
   
    SDL_DestroyTexture(shopTexture);
    SDL_DestroyTexture(fuelStationTexture);
    SDL_DestroyTexture(alienMessageFail);
    SDL_DestroyTexture(alienMessageSuccess);
    SDL_DestroyTexture(money);
    SDL_DestroyTexture(gameLossMessage);
    SDL_DestroyTexture(gameScoreMessage);
    SDL_DestroyTexture(titleMenuCanvas);
    SDL_DestroyTexture(instructionsCanvas);
    SDL_DestroyTexture(backToMenuTex);
    SDL_DestroyTexture(playAgainTex);
    
    Mix_FreeChunk(music);
    Mix_FreeChunk(HelicopterSound);
    Mix_FreeChunk(badEngine);
    Mix_FreeChunk(sickEngine);
    Mix_FreeChunk(successfulPurchaseSound);
    
    music = HelicopterSound = badEngine = sickEngine = successfulPurchaseSound = NULL; // to be safe...
    mineralProcessor->~MineralProcessor();
    fuelStation->~FuelStation();
    healthStation->~HealthShop();
    upgradeShop->~UpgradeShop();
    game-> clean();
    game-> ~Game();
    return 0;
}


//SDL_Texture* runHighScoreConditionsAndCreateTexture();
void runHighScoreConditionsAndCreateTexture(){
    
    //HighScore Conditions:
    if(highScoreResult == nullptr && player->playerScore > previousHighScore) //You got High Score !
    {
        string congratsMessage = "Congratulations you got the new highscore of ";
        string scoreString = to_string(player->playerScore);
        string totalMessage = congratsMessage.append(scoreString);
        const char* totalMessageChar = totalMessage.c_str();
        highScoreResult = TextureManager::CreateTextTexture(totalMessageChar, Game::greenColor);
        if(newHighScoreWritten == false)
        {
            writeNewHighScore(player->playerScore);
        }
    }
    else if(highScoreResult == nullptr && player->playerScore <= previousHighScore)
    {
        string failMessage = "The current highscore still stands as ";
        string scoreString = to_string(previousHighScore);
        string totalMessage = failMessage.append(scoreString);
        const char* totalMessageChar = totalMessage.c_str();
        highScoreResult = TextureManager::CreateTextTexture(totalMessageChar, Game::redColor);
    }
    
}

int getHighScore() {
    //Get High Score to Display:
    ifstream reader;

    reader.open("highScores.txt");
    if(reader.good())
    {
       string content( (std::istreambuf_iterator<char>(reader) ),
                            (std::istreambuf_iterator<char>()    ) );
        int highScore = stoi(content);
        cout<<highScore<<endl;
        return highScore;
    }
    else
        return -1;
}

void writeNewHighScore(int playerHighScore) {
    string newHighScoreString = to_string(playerHighScore);
    std::ofstream writer;
    writer.open("highScores.txt", std::ofstream::out | std::ofstream::trunc);
    writer << newHighScoreString;
    writer.close();
}

void playSound(int channel, Mix_Chunk* sound){
    
    Mix_Volume(DRIVINGCHANNEL, 20);
    Mix_Volume(DRILLINGCHANNEL, 20);
    Mix_Volume(EXPLOSIONCHANNEL, 40);
    Mix_Volume(HELICOPTERCHANNEL, 20);
    int timesToRun = 20;
    if(channel == EXPLOSIONCHANNEL || channel == SUCCESSFULPURCHASECHANNEL)
        timesToRun = 0;

    if(!Mix_Playing(channel))
    {
        if ( Mix_PlayChannel(channel, sound, timesToRun) == -1 )
            std::cerr << "Failed to play effect\n";
        else
            cout<<"success"<<endl;
    }
}
