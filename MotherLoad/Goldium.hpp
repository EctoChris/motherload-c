#ifndef Goldium_hpp
#define Goldium_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Goldium: public Substance {
public:
    int value = 250;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Goldium(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Goldium",  250){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Goldium();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};

#endif /* Goldium_hpp */
