#ifndef SoundManager_hpp
#define SoundManager_hpp

#include <stdio.h>
#include "Game.hpp"
#include <list>

using namespace std;

struct soundListing {
    Mix_Chunk* sound;
    string name;
};

class SoundManager {
public:
    list<soundListing> sounds;
    
    SoundManager();
    ~SoundManager();
    void loadSound(string name, string file);
    void playSound(string name);
    
    static SoundManager soundManager;
    
};
#endif /* SoundManager_hpp */
