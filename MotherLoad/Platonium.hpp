#ifndef Platonium_hpp
#define Platonium_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Platonium: public Substance {
public:
    int value = 750;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Platonium(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Platonium",  750)
    {
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Platonium();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};
#endif /* Platonium_hpp */
