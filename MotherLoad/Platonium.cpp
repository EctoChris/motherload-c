#include "Platonium.hpp"

void Platonium::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Platonium::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = { xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 1, 1};
    
    textTexture = TextureManager::CreateTextTexture("+ 1 Platonium", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Platonium::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
