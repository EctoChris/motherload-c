#include "SoundManager.hpp"


SoundManager SoundManager::soundManager;

SoundManager::SoundManager()
{
    
}

SoundManager::~SoundManager()
{
    //delete all sound effects properly
    for(soundListing listing : sounds)
    {
        Mix_FreeChunk(listing.sound);
    }
}

void SoundManager::loadSound(string name, string file)
{
    soundListing listing;
    listing.name = name;
    listing.sound = Mix_LoadWAV(file.c_str());
    
    sounds.push_back(listing);
}

void SoundManager::playSound(string name)
{
    for (soundListing listing : sounds)
    {
        if(listing.name == name)
        {
            Mix_PlayChannel(-1, listing.sound, 0);
            break;
        }
    }
}
