#ifndef Dirt_hpp
#define Dirt_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Dirt: public Substance {
public:
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Dirt(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Dirt", 0){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Dirt();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};
#endif /* Dirt_hpp */
