#include "Saphire.hpp"

void Saphire::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Saphire::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = { xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 1, 1};
    
    textTexture = TextureManager::CreateTextTexture("+ 1 Saphire", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Saphire::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
