#include "FuelStation.hpp"

enum screenSelection {
    titleScreen = 0,
    gameScreen = 1,
    mineralProcessorScreen = 2,
    fuelStationScreen = 3,
    shopScreen = 4
};


FuelStation::FuelStation(Player* player, Game* game){
    this->player = player;
    this->game = game;
    mainScreen = TextureManager::LoadTexture("NewFuelStation.png");
}

FuelStation::~FuelStation(){
    SDL_DestroyTexture(mainScreen);
}


void FuelStation::generateShopScreen(){
    
    //Drawing UI Canvas:
    SDL_Rect sourceRect = { 10, 10, 10, 10 };
    SDL_Rect destRect = { 50, 50, 800, 500 };
    TextureManager::DrawStill(mainScreen, sourceRect, destRect);
    
    //Drawing Current Fuel Tank:
    //Black Outline of Tank:
    SDL_Rect *tankDestRect = new SDL_Rect();
    tankDestRect->x = 150;
    tankDestRect->y = 190;
    tankDestRect->w = 100;
    tankDestRect->h = 240;
    SDL_SetRenderDrawColor(Game::renderer, 0,0,0, 0);
    SDL_RenderDrawRect(Game::renderer, tankDestRect);
    SDL_RenderFillRect(Game::renderer, tankDestRect);
    
    //Clear Sky Inner of Tank: //add +2x +2y -4W -4H
    tankDestRect->x = 155;
    tankDestRect->y = 195;
    tankDestRect->w = 90;
    tankDestRect->h = 230;
    SDL_SetRenderDrawColor(Game::renderer, 135,206,250, 0);
    SDL_RenderDrawRect(Game::renderer, tankDestRect);
    SDL_RenderFillRect(Game::renderer, tankDestRect);
    
    //Petrol Inside Tank:
    if(player->fuel > 5)
    {
        tankDestRect->y = 425 - player->fuel * 2.3;
        tankDestRect->h = player->fuel * 2.3;
        SDL_SetRenderDrawColor(Game::renderer, 124, 252, 0, 0);
        SDL_RenderDrawRect(Game::renderer, tankDestRect);
        SDL_RenderFillRect(Game::renderer, tankDestRect);
    }
    
    
}
void FuelStation::showSuccessMessage(bool saleWasSucess){
    string amount = to_string(lastRefillAmount);
    string money = to_string(lastRefillCost);
    string totalMessage;
    if(saleWasSucess)
    {
        string message = "You refilled ";
        string message2 = " Litres, costing $";
        totalMessage = message.append(amount).append(message2).append(money);
    }
    else
    {
        string message1 = "Sorry, you cannot afford $";
        string message2 = " for ";
        string message3 = " many litres";
        totalMessage = message1.append(money).append(message2).append(amount).append(message3);
    }
    
    //Drawing GUI Canvas:
    SDL_Texture* successMessage = TextureManager::LoadTexture("SuccessMessage.png");
    SDL_Rect destRect = { 200, 200, 500, 200 };
    SDL_Rect sourceRect = { 10, 10, 10, 10 };
    TextureManager::DrawStill(successMessage, sourceRect, destRect);
    
    //Drawing Text Message:
    destRect = { 230, 240, 400, 50 };
    const char* messageChar = totalMessage.c_str();
    TextureManager::DrawStill(TextureManager::CreateTextTexture(messageChar, Game::greenColor), sourceRect, destRect);
}

void FuelStation::handleInputs(int x, int y){

}

void FuelStation::fillTank(){
    //Conditions for payment:
    lastRefillAmount = 100 - player->fuel;
    lastRefillCost= lastRefillAmount * 2.5;

    if(player->money >= lastRefillCost)
    {
        lastSaleWasSuccess = true;
        showSuccessMessage(true);
        player->fuel = 100;
        player->money -= lastRefillCost;
        
    }
    else if(player->money > 0)
    {
        lastRefillAmount = player->money /4;
        lastRefillCost = player->money;
        player->fuel += lastRefillAmount;
        player->money = 0;
        lastSaleWasSuccess = true;
        showSuccessMessage(true);
    }
    else
    {
        lastSaleWasSuccess = false;
        showSuccessMessage(false);
        return;
    }
}

bool FuelStation::isWithinRange(double testNum, double lowNum, double highNum){
    if (testNum <= highNum && testNum >= lowNum)
        return true;
    return false;
}
