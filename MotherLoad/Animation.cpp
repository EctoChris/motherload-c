#include "Animation.hpp"


Animation::Animation(const char* animationName, int numberOfFrames, int frameWidth, int frameHeight, float speed, int r, int g, int b, bool isHorizontal, int xStartPos, int yStartPos){
    
    SDL_Texture* spriteSheet;
    spriteSheet = TextureManager::LoadTextureAndMakeTransparent(animationName, r, g, b);
    this->spriteSheet = spriteSheet;
    this->numberOfFrames = numberOfFrames;
    this->frameWidth = frameWidth;
    this->frameHeight = frameHeight;
    this->frameDuration = speed;
    this->isHorizontal = isHorizontal;
    this->xStartPos = xStartPos;
    this->yStartPos = yStartPos;
    currentFrame = 0;
    frameTimer = 0;
}

Animation::Animation(){
    
}

void Animation::update(float dt){
    
    frameTimer += dt;

    //If this timer exceeds duration, move onto next frame
    if(frameTimer >= frameDuration)
    {
        currentFrame++;
        //reset the timer
        frameTimer = 0;
        //To Loop animation
        if (currentFrame >= numberOfFrames)
            currentFrame = 0;
    }
}

//Method to run through an animation once only (used for end game animation)
void Animation::runThroughAnimation(float dt){
    frameTimer += dt;
    if(frameTimer >= frameDuration)
    {
        currentFrame++;
        frameTimer = 0;
        if(currentFrame >= numberOfFrames)
            currentFrame = numberOfFrames-1;
    }
}

//Drawing animations
void Animation::draw(int x, int y){
    SDL_Rect sourceRect;
    if(isHorizontal)
    {
        sourceRect.x = currentFrame*frameWidth;
        sourceRect.y = yStartPos;
    }
    else {
        sourceRect.x = xStartPos;
        sourceRect.y = currentFrame*frameHeight;
    }
    sourceRect.w = frameWidth;
    sourceRect.h = frameHeight;
    
    //destination (where do we want to draw this guy)
    SDL_Rect dest = {x, y, frameWidth, frameHeight};
    SDL_RenderCopy(Game::renderer, spriteSheet, &sourceRect, &dest);
}

void Animation::drawStill(int x, int y)
{
    SDL_Rect sourceRext;
    if(isHorizontal)
    {
        sourceRect.x = currentFrame*frameWidth;
        sourceRect.y = yStartPos;
    }
    else {
        sourceRect.x = xStartPos;
        sourceRect.y = currentFrame*frameHeight;
    }
    sourceRect.w = frameWidth;
    sourceRect.h = frameHeight;
    
    //where to draw:
}
void Animation::draw(int x, int y, double scale){
    SDL_Rect sourceRect;
    if(isHorizontal)
    {
        sourceRect.x = currentFrame*frameWidth;
        sourceRect.y = yStartPos;
    }
    else {
        sourceRect.x = xStartPos;
        sourceRect.y = currentFrame*frameHeight;
    }
    sourceRect.w = frameWidth;
    sourceRect.h = frameHeight;
    
    //destination (where do we want to draw this guy)
    SDL_Rect dest = {x, y, static_cast<int>(frameWidth * scale), static_cast<int>(frameHeight * scale)};
    SDL_RenderCopy(Game::renderer, spriteSheet, &sourceRect, &dest);
}
void Animation::draw(int x, int y, bool flip){
    //get current frame clipping region
    SDL_Rect sourceRect;
    if (isHorizontal)
    {
        sourceRect.x = currentFrame*frameWidth;
        sourceRect.y = yStartPos;
    }
    else {
        sourceRect.x = xStartPos;
        sourceRect.y = currentFrame*frameHeight;
    }

    sourceRect.w = frameWidth;
    sourceRect.h = frameHeight;
    //setup where frame will be drawn
    SDL_Rect dest = { x, y, frameWidth, frameHeight };
    
    //get the correct flip flag to help mirror our image if we want to
    SDL_RendererFlip flipType = SDL_FLIP_NONE;
    if (flip)
        flipType = SDL_FLIP_HORIZONTAL;
    
    //draw                                              angle, rotationPoint, flip flag
    SDL_RenderCopyEx(Game::renderer, spriteSheet, &sourceRect, &dest, 0, 0, flipType);
}


//Drawing animations with ability to change both size and direction facing:
void Animation::draw(int x, int y, double scale, bool flip){
    SDL_Rect sourceRect;
    if(isHorizontal)
    {
        sourceRect.x = currentFrame*frameWidth;
        sourceRect.y = yStartPos;
    }
    else {
        sourceRect.x = xStartPos;
        sourceRect.y = currentFrame*frameHeight;
    }
    sourceRect.w = frameWidth;
    sourceRect.h = frameHeight;
    
    //destination (where do we want to draw this guy)
    SDL_Rect dest = {x, y, static_cast<int>(frameWidth * scale), static_cast<int>(frameHeight * scale)};
    
    
    
    SDL_RendererFlip flipType = SDL_FLIP_NONE;
    if (flip)
        flipType = SDL_FLIP_HORIZONTAL;
    
    //draw                                              angle, rotationPoint, flip flag
    SDL_RenderCopyEx(Game::renderer, spriteSheet, &sourceRect, &dest, 0, 0, flipType);
    
}


//Drawing animations with ability to draw animation rotated: (used in flying animation):
void Animation::draw(int x, int y, double scale, const double angle, SDL_Point *rotationPoint, bool flip){
    
    SDL_Rect sourceRect;
    if(isHorizontal)
    {
        sourceRect.x = currentFrame*frameWidth;
        sourceRect.y = yStartPos;
    }
    else {
        sourceRect.x = xStartPos;
        sourceRect.y = currentFrame*frameHeight;
    }
    sourceRect.w = frameWidth;
    sourceRect.h = frameHeight;
    
    //destination (where do we want to draw this guy)
    SDL_Rect dest = {x, y, static_cast<int>(frameWidth * scale), static_cast<int>(frameHeight * scale)};
    
    
    
    SDL_RendererFlip flipType = SDL_FLIP_NONE;
    if (flip)
        flipType = SDL_FLIP_HORIZONTAL;
    
    //draw                                              angle, rotationPoint, flip flag
     SDL_RenderCopyEx(Game::renderer, spriteSheet, &sourceRect, &dest, angle, NULL, flipType);

    
}


Animation::~Animation(){
    
}
