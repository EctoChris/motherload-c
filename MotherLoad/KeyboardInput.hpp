#ifndef KeyboardInput_hpp
#define KeyboardInput_hpp

#include <stdio.h>
#include "Game.hpp"


class KeyboardInput {
public:
    SDL_Scancode UP, DOWN, LEFT, RIGHT;
    KeyboardInput();
    void update(SDL_Event* e);
    bool upKeyPressed = false;
    bool rightKeyPressed = false;
    bool downKeyPressed = false;
    bool leftKeyPressed = false;
    
    
};
#endif /* KeyboardInput_hpp */
