#ifndef HealthShop_hpp
#define HealthShop_hpp

#include <stdio.h>
#include "Player.hpp"
#include <string>

using namespace std;

class HealthShop {
public:
    Player* player;
    Game* game;
    SDL_Texture* successMessage;
    int healthPointsToHeal = 0;
    int healingCost = 0;
    int recentHealthPointsHealed = 0;
    bool showingSuccessMessage = false;
    bool lastHealWasSuccess = false;
    HealthShop(Player* p, Game* g);
    ~HealthShop();
    void generateSuccessMessage(bool healingWasPurchased);
    bool healPlayer();
};
#endif /* HealthShop_hpp */
