#ifndef TextureManager_hpp
#define TextureManager_hpp

#include <stdio.h>
#include "Game.hpp"
#include <SDL2/SDL_ttf.h>


using namespace std;


class TextureManager {
public:
    static SDL_Texture* LoadTexture(const char* fileName);
    static SDL_Texture* LoadTextureAndMakeTransparent(const char* fileName, int r, int g, int b);
    static void Draw(SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect);
    static void Draw(SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect, float scale);
    static void Draw(SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect, bool flip);
    static void DrawStill(SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect);
    static SDL_Texture* CreateTextTexture(const char* message, SDL_Color colour);
   // static void DrawText(SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect);
};

#endif /* TextureManager_hpp */
