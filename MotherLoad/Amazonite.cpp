#include "Amazonite.hpp"

void Amazonite::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Amazonite::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = { xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 1, 1};
    
    textTexture = TextureManager::CreateTextTexture("+ 1 Amazonite", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Amazonite::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
