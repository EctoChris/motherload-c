#ifndef Ironium_hpp
#define Ironium_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Ironium: public Substance {
public:
    int value = 50;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Ironium(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Ironium", 50){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Ironium();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();

};
#endif /* Ironium_hpp */
