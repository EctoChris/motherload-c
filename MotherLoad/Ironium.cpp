#include "Ironium.hpp"


void Ironium::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Ironium::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = {xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 100, 200};
    textTexture = TextureManager::CreateTextTexture("+ 1 Ironium", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Ironium::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
//void updateMessage(float DT){
//
//}
//        TTF_Font* caviarFont = TTF_OpenFont("CaviarDreams.ttf", 16); //font location, font size
//        SDL_Color textColor = {125, 0, 34, 0};
//
//        //Generate surface from font + string:
//        SDL_Surface* textSurface = TTF_RenderText_Blended(caviarFont, "It's nearly Xmas!", textColor);
//
//        //Convert to Texture:
//        SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
//
//        //Delete surface:
//        SDL_FreeSurface(textSurface);
//
//        //Text destination:
//        SDL_Rect textDestination;
//        textDestination.x = 250;
//        textDestination.y = 250;
//
//        //Get width and height of our texture for the destination:
//        SDL_QueryTexture(textTexture, NULL, NULL, &textDestination.w, &textDestination.h);
