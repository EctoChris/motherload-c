#ifndef UpgradeShop_hpp
#define UpgradeShop_hpp

#include <stdio.h>
#include "Player.hpp"
#include <string>

using namespace std;

class UpgradeShop {
public:
    Player* player;
    Game* game;
    SDL_Texture* currentPlayerItem;
    SDL_Texture* nextAvailablePlayerItem;
    SDL_Texture* commonDrill;
    SDL_Texture* rareDrill;
    SDL_Texture* veryRareDrill;
    SDL_Texture* ultraRareDrill;
    SDL_Texture* commonEngine;
    SDL_Texture* rareEngine;
    SDL_Texture* veryRareEngine;
    SDL_Texture* ultraRareEngine;
    SDL_Texture* commonFuelTank;
    SDL_Texture* rareFuelTank;
    SDL_Texture* veryRareFuelTank;
    SDL_Texture* ultraRareFuelTank;
    SDL_Texture* successMessage;
    SDL_Texture* mainScreen;

    int currentItemFocus = 1;
    int currentItemValue;
    int nextUpgradeCost;
    int lastSaleCost = 0;
    bool showingSuccessScreen = false;
    bool lastUpgradeWasSuccess = false;
    string itemName;
    string itemDescription;
    string currentItemFullName;
    string nextUpgradeItemFullName;
    UpgradeShop(Player* p, Game* g);
    ~UpgradeShop();
    void showSuccessMessage();
    void generateShopScreen();
    void changeItemFocus(int selection);
    void handleInputs(const Uint8* keyStates);
    void updateShop(int currentItemValue, int nextUpgradeCost, string currentItemFullName, string nextUpgradeItemName, SDL_Texture* currentPlayerItem, SDL_Texture* nextAvailableUpgrade);
};
#endif /* UpgradeShop_hpp */
