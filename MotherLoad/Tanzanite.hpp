#ifndef Tanzanite_hpp
#define Tanzanite_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Tanzanite: public Substance {
public:
    int value = 30000;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Tanzanite(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Tanzanite",  30000){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Tanzanite();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
    
};


#endif /* Tanzanite_hpp */


