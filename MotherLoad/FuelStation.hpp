#ifndef FuelStation_hpp
#define FuelStation_hpp

#include <stdio.h>
#include "Player.hpp"

using namespace std;

class FuelStation {
public:
    Player* player;
    Game* game;
    SDL_Texture* mainScreen = nullptr;
    bool showingSuccessMessage = false;
    bool lastSaleWasSuccess = false;
    int lastRefillAmount = 0;
    int lastRefillCost = 0;
    FuelStation(Player* player, Game* game);
    ~FuelStation();
    void generateShopScreen();
    void showSuccessMessage(bool lastSaleWasSuccess);
    void handleInputs(int x, int y);
    void fillTank();
    bool isWithinRange(double testNum, double lowNum, double highNum);
    
};
#endif /* FuelStation_hpp */
