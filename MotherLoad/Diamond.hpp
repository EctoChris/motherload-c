#ifndef Diamond_hpp
#define Diamond_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Diamond: public Substance {
public:
    int value = 5000;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Diamond(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Diamond",  5000){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Diamond();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};

#endif /* Diamond_hpp */
