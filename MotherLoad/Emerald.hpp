#ifndef Emerald_hpp
#define Emerald_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Emerald: public Substance {
public:
    int value = 1500;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Emerald(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Emerald",  1500){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Emerald();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};
#endif /* Emerald_hpp */
