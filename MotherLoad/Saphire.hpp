#ifndef Saphire_hpp
#define Saphire_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Saphire: public Substance {
public:
    int value = 10000;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Saphire(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Saphire",  10000){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Saphire();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};
#endif /* Saphire_hpp */
