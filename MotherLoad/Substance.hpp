#ifndef Substance_hpp
#define Substance_hpp

#include <stdio.h>
#include "Game.hpp"

using namespace std;

class Substance {
public:
    int xPos;
    int yPos;
    int value;
    bool diggable = true;
    const char* substanceType;
    int diggingDifficulty;
    float deathTimer;
    SDL_Rect collisionBox;  //Box describing the size of our entity and this is used to bump into things
    SDL_Texture* texture;
    SDL_Texture* textTexture;
    SDL_Rect textDestination;
    Substance(int xPos, int yPos, SDL_Texture* texture, SDL_Rect destRect, const char* substanceType, int value);
    ~Substance();
    virtual void Draw();
    virtual void ShowSuccessMessage(int xPos, int yPos, float DT);
    virtual void updateMessage();
    void startDeathTimer();
};

#endif /* Substance_hpp */
