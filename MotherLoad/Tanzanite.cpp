#include "Tanzanite.hpp"

void Tanzanite::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Tanzanite::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = { xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 1, 1};
    
    textTexture = TextureManager::CreateTextTexture("+ 1 Tanzanite", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Tanzanite::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
