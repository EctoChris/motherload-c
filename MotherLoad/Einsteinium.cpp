#include "Einsteinium.hpp"


void Einsteinium::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Einsteinium::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = { xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 1, 1};
    
    textTexture = TextureManager::CreateTextTexture("+ 1 Einsteinium", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Einsteinium::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
