#include "CameraManager.hpp"


void CameraManager::update(){
    
    if(player != nullptr)
    {
        Game::camera.x = player->xPos - Game::screenWidth / 2;
        Game::camera.y = player->yPos - Game::screenHeight / 2;
        
        if(Game::camera.x < 5)
            Game::camera.x = 0;
        
        if(Game::camera.x > Game::LEVEL_WIDTH - Game::screenWidth - 5)
            Game::camera.x = Game::LEVEL_WIDTH - Game::screenWidth;
        
        //testing bottom of map:
        if(Game::camera.y > Map::lavaBedRockCollisionBox.y - Game::screenHeight + 50)
            Game::camera.y = Map::lavaBedRockCollisionBox.y - Game::screenHeight + 45;
    }
}
