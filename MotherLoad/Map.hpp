#ifndef Map_hpp
#define Map_hpp

#include "Game.hpp"
#include <cstdlib>
#include <ctime>
#include "Dirt.hpp"
#include "Bronzium.hpp"
#include "Ironium.hpp"
#include "Goldium.hpp"
#include "Emerald.hpp"
#include "Einsteinium.hpp"
#include "Ruby.hpp"
#include "Diamond.hpp"
#include "Amazonite.hpp"
#include "Platonium.hpp"
#include "Player.hpp"
#include "Saphire.hpp"
using namespace std;

class Map {
public:
    static list<Substance*> substances;
    static SDL_Rect lavaBedRockCollisionBox;
    SDL_Rect shopCollisionBox, fuelStationCollisionBox, upgradeShopCollisionBox;
    Map();
    ~Map();
    void addToSubstances(Substance* substance);
    void LoadMap();
    void DrawMap();
    void UpdateMap(float dt);
    void DrawUpperLevel();
    void DrawMiddleLevel();
    void DrawLowerLevel();
    void DrawDarkLevel();
    void DrawAlien(float dt);
    void DrawSpaceShip();
    void DrawAlienRunning(float dt);
    void DrawSpaceShipFlying(float dt, bool timeToFly);
    void updateFlyingSpaceShip(float dt);
    Animation alienIdle, endGameRocket, floatingPaper, alienRunning;

private:
    SDL_Rect sourceRect, destRect;
    SDL_Texture *dirt;
    SDL_Texture *ironium;
    SDL_Texture *bronzium;
    SDL_Texture *goldium;
    SDL_Texture *mineralProcessor;
    SDL_Texture *FuelStation;
    SDL_Texture *einsteinium;
    SDL_Texture *platonium;
    SDL_Texture *emerald;
    SDL_Texture *ruby;
    SDL_Texture *diamond;
    SDL_Texture *amazonite;
    SDL_Texture *lavaBedRock;
    SDL_Texture *upgradeShop;
    SDL_Texture *healthStation;
    SDL_Texture *saphire;
    float alienAnimationXPos;
    float alienAnimationYPos;
    float spaceShipAnimationXPos;
    float spaceShipAnimationYPos;
    int map[25][50];
    int middleLevel[25][50];
    bool timeToFly = false;
    bool alienIsRunning = false;
};

#endif /* Map_hpp */
