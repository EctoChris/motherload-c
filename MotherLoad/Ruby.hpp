#ifndef Ruby_hpp
#define Ruby_hpp

#include <stdio.h>
#include "Substance.hpp"
#include "Game.hpp"

using namespace std;

class Ruby: public Substance {
public:
    int value = 2000;
    int difficultyToDestroy = 1;
    SDL_Rect sourceRect, destRect;
    Ruby(int xPos, int yPos, SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect) : Substance(xPos, yPos, texture, destRect, "Ruby",  2000){
        TextureManager::Draw(texture, sourceRect, destRect);
        this->sourceRect = sourceRect;
        this->destRect = destRect;
    }
    ~Ruby();
    void Draw();
    void ShowSuccessMessage(int xPos, int yPos, float DT);
    void updateMessage();
};

#endif /* Ruby_hpp */
