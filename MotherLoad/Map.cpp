#include "Map.hpp"

list<Substance*>Map::substances = list<Substance*>();
SDL_Rect Map::lavaBedRockCollisionBox = { -100, 3942, 2900, 80 };

const int blockSize = 60; //50
Map::Map()
{
    //Initialize all Textures
    srand(time(NULL));
    alienIdle = Animation("AlienFinished2.png", 9, 96, 150, 0.15, 255,255,255, true, 0, 0);
    FuelStation = TextureManager::LoadTexture("FuelStationIsland.png");
    mineralProcessor = TextureManager::LoadTexture("MineralProcessingIsland.png");
    upgradeShop = TextureManager::LoadTexture("UpgradeShopFinished.png");
    dirt = TextureManager::LoadTexture("Dirt.png");
    ironium = TextureManager::LoadTexture("Ironium.png");
    bronzium = TextureManager::LoadTexture("Bronzium.png");
    goldium = TextureManager::LoadTexture("Goldium.png");
    amazonite = TextureManager::LoadTexture("Amazonite.png");
    diamond = TextureManager::LoadTexture("Diamond.png");
    saphire = TextureManager::LoadTexture("Saphire3.png");
    emerald = TextureManager::LoadTexture("Emerald.png");
    platonium = TextureManager::LoadTexture("Platonium.png");
    ruby = TextureManager::LoadTexture("ruby.png");
    einsteinium = TextureManager::LoadTexture("Einsteinium.png");
    lavaBedRock = TextureManager::LoadTexture("BedRock.png");
    healthStation = TextureManager::LoadTexture("HealthIslandFinished.png");
    endGameRocket = Animation("EndGameRocket.png", 7, 415, 490, 0.25, 255,255,255, true, 0, 0);
    floatingPaper = Animation("FloatingPaperSprite.png",4, 325, 330, 0.4, 255,255,255, true, 0, 0);
    alienRunning = Animation("AlienFinished2.png", 6, 96, 150, 0.15, 255,255,255, true, 0, 150);
    alienAnimationXPos = 2150;
    alienAnimationYPos = 350;
    spaceShipAnimationXPos = 2160;
    spaceShipAnimationYPos = 182;
    
    //Create a 2D Array for the map with random numbers between 0 - 25
    for (int row = 0; row<25; row++)
    {
        for(int col = 0; col < 50; col++)
        {
            map[row][col] = (rand() % 25);
            middleLevel[row][col] = (rand() % 25);
        }
    }
    sourceRect.x = sourceRect.y = 0;
    sourceRect.w = destRect.w = blockSize;
    sourceRect.h = destRect.h = blockSize;
}


//Deconstructure to clear memory
Map::~Map(){
    Map::substances.clear();
    SDL_DestroyTexture(dirt);
    SDL_DestroyTexture(ironium);
    SDL_DestroyTexture(bronzium);
    SDL_DestroyTexture(goldium);
    SDL_DestroyTexture(mineralProcessor);
    SDL_DestroyTexture(FuelStation);
    SDL_DestroyTexture(einsteinium);
    SDL_DestroyTexture(platonium);
    SDL_DestroyTexture(emerald);
    SDL_DestroyTexture(ruby);
    SDL_DestroyTexture(diamond);
    SDL_DestroyTexture(amazonite);
    SDL_DestroyTexture(lavaBedRock);
    SDL_DestroyTexture(upgradeShop);
    SDL_DestroyTexture(healthStation);
    SDL_DestroyTexture(saphire);
}

void Map::LoadMap(){
    
}

//Function to draw all sections of map
void Map::DrawMap(){
    DrawUpperLevel();
    DrawMiddleLevel();
    DrawLowerLevel();
    DrawDarkLevel();
}

//Draw end game alien
void Map::DrawAlien(float DT){
    alienIdle.update(DT);
    alienIdle.draw(2150 - Game::camera.x , 350 - Game::camera.y, 0.5, true);
    floatingPaper.update(DT);
    floatingPaper.draw(2180 - Game::camera.x, 310 - Game::camera.y, 0.2);
}

//Draw End game alien running (used in end game animation)
void Map::DrawAlienRunning(float DT){
    alienIsRunning = true;
    alienRunning.update(DT);
    alienRunning.draw(alienAnimationXPos - Game::camera.x, alienAnimationYPos - Game::camera.y, 0.5, false);
    alienAnimationXPos+= 0.1;
}

//Draw end game space ship flying (used in end game animation)
void Map::DrawSpaceShipFlying(float DT, bool timeToFly)
{
    if(timeToFly)
    {
        this->timeToFly = true;
        endGameRocket.runThroughAnimation(DT);
        endGameRocket.draw(spaceShipAnimationXPos - Game::camera.x, spaceShipAnimationYPos - Game::camera.y, 0.9);
    }
    else
        endGameRocket.draw(spaceShipAnimationXPos - Game::camera.x, spaceShipAnimationYPos - Game::camera.y, 0.9);
}

//Update position of ship during end game animation
void Map::updateFlyingSpaceShip(float DT)
{
    if(endGameRocket.currentFrame <= 3)
        spaceShipAnimationYPos-= 0.03;
    else if(endGameRocket.currentFrame >3 && endGameRocket.currentFrame < 6)
        spaceShipAnimationYPos-= 0.1;
    else
        spaceShipAnimationYPos-= 0.2;
}

//Draw spaceship
void Map::DrawSpaceShip()
{
    endGameRocket.draw(2160  - Game::camera.x, 182 - Game::camera.y, 0.9);
}

//Drawing first section of map based off randomized numbers initialized in constructor
void Map::DrawUpperLevel(){
    int type = 0;
    for(int row = 0; row < 25; row++)
    {
        for (int col = 0; col < 50; col++)
        {
            type= map[row][col];
            
            destRect.x = col * blockSize;
            destRect.y = row * blockSize;
            if(row > 6)
            {
                switch(type)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    {
                        Dirt* d = new Dirt(sourceRect.x, sourceRect.y, dirt, sourceRect, destRect);
                        addToSubstances((Substance*)d);
                        break;
                    }
                    case 15:
                    case 16:
                    {
                        Bronzium* b = new Bronzium(sourceRect.x, sourceRect.y, bronzium, sourceRect, destRect);
                        addToSubstances((Substance*)b);
                        break;
                    }
                    case 17:
                    case 18:
                    {
                        Ironium* i = new Ironium(sourceRect.x, sourceRect.y, ironium, sourceRect, destRect);
                        addToSubstances((Substance*)i);
                        break;
                    }
                    case 19:
                    {
                        Goldium* g = new Goldium(sourceRect.x, sourceRect.y, goldium, sourceRect, destRect);
                        addToSubstances((Substance*)g);
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}

//Drawing 2nd section of map (lower section with rarer substances) based off randomized numbers initialized in constructor
void Map::DrawMiddleLevel(){
    
    int type = 0;
    
    for(int row = 0; row < 25; row++)
    {
        for (int col = 0; col < 50; col++)
        {
            type= map[row][col];
            
            destRect.x = col * blockSize;
            //destRect.y = 900 + row * blockSize;
            destRect.y = 1080 + row * blockSize;
            if(row > 6)
            {
                switch(type)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 16:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    {
                        Dirt* d = new Dirt(sourceRect.x, sourceRect.y, dirt, sourceRect, destRect);
                        addToSubstances((Substance*)d);
                        break;
                    }
                    case 14:
                    {
                        Goldium* g = new Goldium(sourceRect.x, sourceRect.y, goldium, sourceRect, destRect);
                        addToSubstances((Substance*)g);
                        break;
                    }
                    case 15:
                    {
                        Ironium* i = new Ironium(sourceRect.x, sourceRect.y, ironium, sourceRect, destRect);
                        addToSubstances((Substance*)i);
                        break;
                    }
                    case 17:
                    {
                        Bronzium* b = new Bronzium(sourceRect.x, sourceRect.y, bronzium, sourceRect, destRect);
                        addToSubstances((Substance*)b);
                        break;
                    }
                    case 18:
                    {
                        
                        Platonium* p = new Platonium(sourceRect.x, sourceRect.y, platonium, sourceRect, destRect);
                        addToSubstances((Substance*)p);
                        break;
                    }
                    case 19:
                    {
                        Einsteinium* ei = new Einsteinium(sourceRect.x, sourceRect.y, einsteinium, sourceRect, destRect);
                        addToSubstances((Substance*)ei);
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}

//Drawing 3rd section of map (lower section with rarer substances) based off randomized numbers initialized in constructor
void Map::DrawLowerLevel(){
    int type = 0;
    
    for(int row = 0; row < 25; row++)
    {
        for (int col = 0; col < 50; col++)
        {
            type= map[row][col];
            
            destRect.x = col * blockSize;
           // destRect.y = 1800 + row * blockSize;
            destRect.y = 2160 + row * blockSize;
            if(row > 6)
            {
                switch(type)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 20:
                    case 21:
                    case 22:
                    {
                        Dirt* d = new Dirt(sourceRect.x, sourceRect.y, dirt, sourceRect, destRect);
                        addToSubstances((Substance*)d);
                        break;
                    }
                    case 14:
                    {
                        Goldium* g = new Goldium(sourceRect.x, sourceRect.y, goldium, sourceRect, destRect);
                        addToSubstances((Substance*)g);
                        break;
                    }
                    case 15:
                    {
                        Ironium* i = new Ironium(sourceRect.x, sourceRect.y, ironium, sourceRect, destRect);
                        addToSubstances((Substance*)i);
                        break;
                    }
                    case 16:
                    {
                        Emerald* e = new Emerald(sourceRect.x, sourceRect.y, emerald, sourceRect, destRect);
                        addToSubstances((Substance*)e);
                        break;
                    }
                    case 17:
                    {
                        Bronzium* b = new Bronzium(sourceRect.x, sourceRect.y, bronzium, sourceRect, destRect);
                        addToSubstances((Substance*)b);
                        break;
                    }
                    case 18:
                    {
                        
                        Platonium* p = new Platonium(sourceRect.x, sourceRect.y, platonium, sourceRect, destRect);
                        addToSubstances((Substance*)p);
                        break;
                    }
                    case 19:
                    {
                        Einsteinium* ei = new Einsteinium(sourceRect.x, sourceRect.y, einsteinium, sourceRect, destRect);
                        addToSubstances((Substance*)ei);
                        break;
                    }
                    case 24:
                    {
                        Ruby* r = new Ruby(sourceRect.x, sourceRect.y, ruby, sourceRect, destRect);
                        addToSubstances((Substance*)r);
                        break;
                    }
                    case 23:
                    {
                        //Treasure:
                        Dirt* d = new Dirt(sourceRect.x, sourceRect.y, dirt, sourceRect, destRect);
                        addToSubstances((Substance*)d);
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
    
}

//Drawing 4th section of map (lower section with rarer substances) based off randomized numbers initialized in constructor
void Map::DrawDarkLevel(){
    int type = 0;
    
    for(int row = 0; row < 25; row++)
    {
        for (int col = 0; col < 50; col++)
        {
            type= map[row][col];
            
            destRect.x = col * blockSize;
           // destRect.y = 2700 + row * blockSize;
            destRect.y = 3240 + row * blockSize;
            if(row > 6)
            {
                switch(type)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 20:
                    case 22:
                    {
                        Dirt* d = new Dirt(sourceRect.x, sourceRect.y, dirt, sourceRect, destRect);
                        addToSubstances((Substance*)d);
                        break;
                    }
                    case 14:
                    {
                        Goldium* g = new Goldium(sourceRect.x, sourceRect.y, goldium, sourceRect, destRect);
                        addToSubstances((Substance*)g);
                        break;
                    }
                    case 15:
                    {
                        Ironium* i = new Ironium(sourceRect.x, sourceRect.y, ironium, sourceRect, destRect);
                        addToSubstances((Substance*)i);
                        break;
                    }
                    case 16:
                    {
                        Emerald* e = new Emerald(sourceRect.x, sourceRect.y, emerald, sourceRect, destRect);
                        addToSubstances((Substance*)e);
                        break;
                    }
                    case 17:
                    {
                        Bronzium* b = new Bronzium(sourceRect.x, sourceRect.y, bronzium, sourceRect, destRect);
                        addToSubstances((Substance*)b);
                        break;
                    }
                    case 18:
                    {
                        
                        Platonium* p = new Platonium(sourceRect.x, sourceRect.y, platonium, sourceRect, destRect);
                        addToSubstances((Substance*)p);
                        break;
                    }
                    case 19:
                    {
                        Einsteinium* ei = new Einsteinium(sourceRect.x, sourceRect.y, einsteinium, sourceRect, destRect);
                        addToSubstances((Substance*)ei);
                        break;
                    }
                    case 21:
                    {
                        Saphire* sa = new Saphire(sourceRect.x, sourceRect.y, saphire, sourceRect, destRect);
                        addToSubstances((Substance*)sa);
                        break;
                    }
                    case 24:
                    {
                        Ruby* r = new Ruby(sourceRect.x, sourceRect.y, ruby, sourceRect, destRect);
                        addToSubstances((Substance*)r);
                        break;
                    }
                    case 23:
                    {
                        Amazonite* a = new Amazonite(sourceRect.x, sourceRect.y, amazonite, sourceRect, destRect);
                        addToSubstances((Substance*)a);
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}

void Map::UpdateMap(float dt){
    for(const auto& Substance : Map::substances)
    {
        Substance->Draw();
    }
    
    //Drawing Mineral Processor Island:
    destRect = {0, 40, 240, 150 };
    TextureManager::Draw(mineralProcessor, sourceRect, destRect);
    shopCollisionBox = {0, 130, 240, 10};
    
    //Drawing Fuel Station Island:
    destRect = {650, 50, 240, 140 };
    TextureManager::Draw(FuelStation, sourceRect, destRect);
    fuelStationCollisionBox = { 650, 80, 240, 10};
    
    //Drawing Upgrade Shop:
    destRect = {1100, 50, 240, 140 };
    TextureManager::Draw(upgradeShop, sourceRect, destRect);
    upgradeShopCollisionBox = { 1100, 80, 240, 10 };
    
    
    //Drawing Health Station:
    destRect = { 1550, 50, 240, 140 };
    TextureManager::Draw(healthStation, sourceRect, destRect);
    
    //Draw Lava Bedrock:
    destRect = { -100, 4660, 1400, 80 };
    lavaBedRockCollisionBox = { -100, 4660, 1400, 80 };
    TextureManager::Draw(lavaBedRock, sourceRect, destRect);
    destRect = {1200, 4655, 1500, 80 };
    TextureManager::Draw(lavaBedRock, sourceRect, destRect);
    
    if(!alienIsRunning)
        DrawAlien(dt);
    if(!timeToFly)
        DrawSpaceShip();
}

void Map::addToSubstances(Substance* substance){
    substances.push_front(substance);
}

