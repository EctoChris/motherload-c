#include "Player.hpp"

enum animation {
    idle = 0,
    driveRight = 1,
    driveLeft = 2,
    drillRight = 3,
    drillLeft = 4,
    drillDown = 5,
    flyUp = 6,
    flyRight = 7,
    flyLeft = 8,
    idleLeft = 9,
    flyUpLeft = 10,
    drillDownLeft = 11,
    explosion = 12
};

enum keyboardInput {
    UP = 0,
    DOWN = 1,
    LEFT = 2,
    RIGHT = 3,
    TOPRIGHT = 4,
    BOTTOMRIGHT = 5,
    BOTTOMLEFT = 6,
    TOPLEFT = 7,
    SPACE = 8,
    FALL = 9,
    DRILLDOWN = 10,
    DRILLRIGHT = 11,
    DRILLLEFT = 12,
    CONTINUEDRILLING = 13
};

enum channel {
    EXPLOSIONCHANNEL = 0,
    DRILLINGCHANNEL = 1,
    HELICOPTERCHANNEL = 2,
    DRIVINGCHANNEL = 3,
    SUCCESSFULPURCHASECHANNEL = 4,
    MUSICCHANNEL = 5,
    DINGCHANNEL = 6
};

bool isWithinRange(double testNum, double lowNum, double highNum);
const double xCollisionMultiplier = 0.8;
const double yCollisionMultiplier = 0.9;

list<Substance*>Player::inventory = list<Substance*>();

Animation vehicleMove = Animation("motherloadSpriteFinished.png", 4, 825, 750, 0.05, 255,255,255, true, 0, 0);
Animation vehicleFly = Animation("motherloadSpriteFinished.png", 9, 825, 750, 0.02, 255,255,255, true, 0, 812);
Animation vehicleDrillHorizontal = Animation("motherloadSpriteFinished.png", 4, 825, 750, 0.05, 255,255,255, true, 0, 1642);
Animation vehicleDrillDown = Animation("motherloadSpriteFinished.png", 4, 825, 750, 0.05, 255,255,255, true, 0, 2470);
Animation vehicleIdle = Animation("motherloadSpriteFinished.png", 4, 825, 750, 0.05, 255,255,255, true, 0, 3300);

Player::Player(int startXPos, int startYPos, double vehicleSize, Mix_Chunk* substanceSound){
    
    //initial object positions:
    xPos = startXPos;
    yPos = startYPos;
    this->collisionBox = new SDL_Rect;
    this->collisionBox->x = xPos + (xCollisionMultiplier * 825) * vehicleSize + 500;
    this->collisionBox->y = yPos + (yCollisionMultiplier * 750) * vehicleSize;
    this->collisionBox->h = (0.6893 * 750) * vehicleSize;
    this->collisionBox->w = (0.40 * 825) * vehicleSize;
    this->vehicleSize = vehicleSize;
    this->foundSubstanceSound = substanceSound;
    currentAnimation = 8;
    lastDirectionFaced = LEFT;
    velocity.x = 0;
    velocity.y = 0;
    
    heart = TextureManager::LoadTexture("Heart.png");
    whiteHeart = TextureManager::LoadTexture("WhiteHeart.png");
}

Player::~Player(){
    SDL_DestroyTexture(objTexture);
    SDL_DestroyTexture(heart);
    SDL_DestroyTexture(whiteHeart);
}

void Player::update(float DT)
{
    
}

void Player::Render()
{
    
}


void Player::draw(){

}

void Player::updateMovement(float DT){

    if(velocity.y > 0)
    {
        velocity.y -= DT * 50;
    }
    
    if(velocity.x > 0)
    {
        velocity.x -= DT * 50;
    }
    if (velocity.x < 0)
    {
        velocity.x += DT * 50;
    }
    
    xPos = xPos + (velocity.x * DT);
    yPos = yPos + (velocity.y * DT);
}

//Check if player moving fast enough to warrant fall damage
void Player::checkForFallDamage(){
    if(velocity.x > 400)
    {
        if(checkCollisionBelow(1) == true)
        {
            playerHealth -= velocity.x / 35;
            damageFlash();
        }
    }
    if(velocity.y > 300 && velocity.y <= 380)
    {
        if(checkCollisionBelow(1) == true)
        {
            playerHealth -= velocity.y / 25;
            damageFlash();
        }
    }
    else if(velocity.y > 380)
    {
        if(checkCollisionBelow(1) == true)
        {
            playerHealth -= velocity.y / 6;
            damageFlash();
        }
    }
}

//Move player in certain direction based off it's current action
void Player::move(int action){
    switch(action)
    {
        case UP:
        {
            velocity.y-=5;
            updateCollisionBox();
            break;
        }
        case DOWN:
        {
            if(!checkCollisionBelow(currentAnimation)){
                yPos+= 5;
                updateCollisionBox();
            }
            break;
        }
        case LEFT:
        {
            if(!checkCollisionLeft())
            {
                if(checkCollisionBelow(currentAnimation) || checkPlayerOnNonSubstanceSurface())
                {
                    xPos -= engineSpeed;
                }
            }
            else {
                velocity.x = 0;
                
            }
            updateCollisionBox();
            break;
        }
        case RIGHT:
        {
            if(!checkCollisionRight())
            {
                if(checkCollisionBelow(currentAnimation) || checkPlayerOnNonSubstanceSurface())
                {
                    xPos += engineSpeed;
                }
            }
            else {
                velocity.x = 0;
            }
            updateCollisionBox();
            break;
        }
        case TOPRIGHT:
        {
            if(!checkCollisionRight())
                velocity.x += 12;
            if(!checkCollisionAbove())
                velocity.y -= 5;
            updateCollisionBox();
            break;
        }
        case TOPLEFT:
        {
            if(!checkCollisionLeft())
                velocity.x -= 12;
            if(!checkCollisionAbove())
                velocity.y -= 5;
            updateCollisionBox();
            break;
        }
        case FALL:
        {
            if(!checkCollisionBelow(currentAnimation) && !checkPlayerOnNonSubstanceSurface()){
                velocity.y += 5 * gravity;
                updateCollisionBox();
            }
            else {
                velocity.y = 0;
                velocity.x = 20;
                updateCollisionBox();
            }
            break;
        }
        case DRILLDOWN:
        {
            drillInDirection(DOWN);
            currentDrillingDirection = DOWN;
            currentlyDrilling = true;
            break;
        }
        case DRILLLEFT:
        {
            drillInDirection(LEFT);
            currentDrillingDirection = LEFT;
            currentlyDrilling = true;
            break;
        }
        case DRILLRIGHT:
        {
            drillInDirection(RIGHT);
            currentDrillingDirection = RIGHT;
            currentlyDrilling = true;
            break;
        }
        case CONTINUEDRILLING:
        {
            if (distanceDrilled < 50)
            {
                distanceDrilled += drillSpeed;
                switch(currentDrillingDirection)
                {
                    case DOWN:
                    {
                        //If drilling down, and not perfectly in center of the substance below, as you are drilling, slowly correct course
                        yPos += drillSpeed;
                        double xCenterOfPlayer = collisionBox->x + (0.5 * collisionBox->w);
                        double xCenterOfSubstance = substanceBeingDrilled->collisionBox.x + (0.5 * substanceBeingDrilled->collisionBox.w);
                        if (!isWithinRange(xCenterOfPlayer, xCenterOfSubstance-1, xCenterOfSubstance+1))
                        {
                            if (xCenterOfPlayer > xCenterOfSubstance)
                            {
                                xPos -= 1;
                                updateCollisionBox();
                            }
                            else
                            {
                                xPos += 1;
                                updateCollisionBox();
                            }
                        }
                        break;
                    }
                    case LEFT:
                        xPos-= drillSpeed;
                        updateCollisionBox();
                        break;
                    case RIGHT:
                        xPos+= drillSpeed;
                        updateCollisionBox();
                        break;
                    default:
                        break;
                }
               
            }
            else {
                distanceDrilled = 0;
                currentlyDrilling = false;
                inventory.push_back(substanceBeingDrilled);
                //Add substance to inventory of player once player has finished drilling the substance
               // substanceBeingDrilled->ShowSuccessMessage(substanceBeingDrilled->collisionBox.x, substanceBeingDrilled->collisionBox.y,DT);
                cout<<inventory.size()<< " many substances in inventory"<<endl;
                //Keep track of previous substance being built for points message to float above
                if(substanceBeingDrilled != nullptr)
                {
                        lastSubstanceBeingDrilled = substanceBeingDrilled;
                }
                
                //Add score to player based on the rarity of the substance
                if(substanceBeingDrilled->substanceType == "Ironium")
                {
                    playerScore += 10;
                }
                else if (substanceBeingDrilled->substanceType == "Bronzium")
                {
                    playerScore += 20;
                }
                else if(substanceBeingDrilled->substanceType == "Goldium")
                {
                    playerScore += 50;
                }
                else if(substanceBeingDrilled->substanceType == "Einsteinium")
                {
                    playerScore += 70;
                }
                else if (substanceBeingDrilled->substanceType == "Platonium")
                {
                    playerScore += 70;
                }
                else if (substanceBeingDrilled->substanceType == "Emerald")
                {
                    playerScore += 100;
                }
                else if (substanceBeingDrilled->substanceType == "Ruby")
                {
                    playerScore += 120;
                }
                else if (substanceBeingDrilled->substanceType == "Amazonite")
                {
                    playerScore += 150;
                }
                destroySubstance();
            }
            break;
        }
        default:
            break;
    }
}

//Change player collisionbox to be where it's x + y pos are located
void Player::updateCollisionBox(){
    this->collisionBox->x = xPos + (0.4 * 825) * vehicleSize + 5;
    this->collisionBox->y = yPos + (0.9 * 750) * vehicleSize;
}

void Player::changeAnimation(int newState, bool resetFrameToBeginning){
    
}


bool Player::checkCollisionBelow(int action){
    
    //loop through all map substances:
    for(const auto& Substance : Map::substances)
    {
        //is substance.y directly below player?
        if(isSubstanceBelowPlayer(Substance))
        {
            //is left bottom corner of vehicle in alignment with substance.x?
            if (isWithinRange(collisionBox->x, Substance->collisionBox.x, Substance->collisionBox.x + Substance->collisionBox.w))
            {
                onMineralProcessorIsland = false;
                onFuelStationIsland = false;
                onHealthStation = false;
                onUpgradeShopIsland = false;
                return true;
            }
            
            //is right bottom corner of vehicle in alignment with substance.x?
            if (isWithinRange(collisionBox->x + collisionBox->w, Substance->collisionBox.x, Substance->collisionBox.x + Substance->collisionBox.w))
            {
                onMineralProcessorIsland = false;
                onFuelStationIsland = false;
                onHealthStation = false;
                onUpgradeShopIsland = false;
                return true;
            }
        }
    }
    return false;
}

bool Player::checkCollisionAbove(){
    for(const auto& Substance : Map::substances)
    {
        if(isSubstanceAbovePlayer(Substance))
        {
            //is left bottom corner of vehicle in alignment with substance.x?
            if (isWithinRange(collisionBox->x, Substance->collisionBox.x, Substance->collisionBox.x + Substance->collisionBox.w))
                return true;
            
            //is right bottom corner of vehicle in alignment with substance.x?
            if (isWithinRange(collisionBox->x + collisionBox->w, Substance->collisionBox.x, Substance->collisionBox.x + Substance->collisionBox.w))
                return true;
        }
    }
    
    //Is MineralProcessing Shop Above Player?:
    if(isWithinRange(collisionBox->x, 0, 240))
    {
        if(isWithinRange(collisionBox->y, 130, 140))
            return true;
    }
    
    //Is FuelStation Above Player?:
    if(isWithinRange(collisionBox->x, 640, 900))
    {
        if(isWithinRange(collisionBox->y , 140, 150))
        {
            return true;
        }
    }
    
    //Is Upgrade Shop Above Player?:
    if(isWithinRange(collisionBox->x, 1100, 1350))
    {
        if(isWithinRange(collisionBox->y, 140, 150))
        {
            return true;
        }
    }
    
    //Is Health Station Above Player?:
    if(isWithinRange(collisionBox->x, 1550, 1790))
    {
        if(isWithinRange(collisionBox->y, 150, 160))
        {
            return true;
        }
    }
    
    return false;
}

bool Player::checkPlayerOnNonSubstanceSurface(){
    //Is Mineral Processor island below player?
    if(isWithinRange(collisionBox->x + (0.5 * collisionBox->w), 0, 240))
    {
        if(isWithinRange(collisionBox->y+collisionBox->h, 130, 140))
        {
            onMineralProcessorIsland = true;
            return true;
        }
    }
    
    //is FuelStation island below player?
    if(isWithinRange(collisionBox->x + (0.5 * collisionBox->w), 640, 900))
    {
        if(isWithinRange(collisionBox->y+collisionBox->h, 140, 150)){
            onFuelStationIsland = true;
            return true;
        }
    }
    
    //is Upgrade Shop Island below player ?
    if(isWithinRange(collisionBox->x + (0.5 * collisionBox->w), 1100, 1350))
    {
        if(isWithinRange(collisionBox->y + collisionBox->h, 140, 150))
        {
            onUpgradeShopIsland = true;
            return true;
        }
    }
    
    //Is Health station below player ?
    if (isWithinRange(collisionBox->x + (0.5 * collisionBox->w), 1550, 1790))
    {
        if(isWithinRange(collisionBox->y + collisionBox->h, 150, 160))
        {
            onHealthStation = true;
            return true;
        }
    }
    
    //is lavaBedRock below player?
    if(isWithinRange(collisionBox->y + collisionBox->h, Map::lavaBedRockCollisionBox.y- 5, Map::lavaBedRockCollisionBox.y + 40))
        return true;
    
    onMineralProcessorIsland = false;
    onFuelStationIsland = false;
    onHealthStation = false;
    onUpgradeShopIsland = false;
    return false;
}

bool Player::checkCollisionLeft(){
    
    //Loop through all map substances
    for(const auto& Substance : Map::substances)
    {
        //is substance directly left ?
        if(isSubstanceLeftOfPlayer(Substance))
        {
            return true;
        }
    }
    return false;
}

bool Player::checkCollisionRight(){
    
    //Loop through all map substances
    for(const auto& Substance : Map::substances)
    {
        //is substance directly right ?
        if(isSubstanceRightOfPlayer(Substance))
        {
            return true;
        }
    }
    return false;
}

void Player::drillInDirection(int direction){
    Substance* substance = nullptr;
    //At this point we already know there is a substance either left, right or below the player
    switch(direction)
    {
        case DOWN:
        {
            //Retrieve substance below player and drill into it
            substance = getSubstanceDirectlyBelow();
            substanceBeingDrilled = substance;
            drillIntoSubstance(substance, DOWN);
            break;
        }
        case LEFT:
        {
            //Retrieve substance left of player and drill into it
            substance = getSubstanceToLeft();
            substanceBeingDrilled = substance;
            drillIntoSubstance(substance, LEFT);
            break;
        }
        case RIGHT:
        {
            //Retrieve sustance right of player and drill into it
            substance = getSubstanceToRight();
            substanceBeingDrilled = substance;
            drillIntoSubstance(substance, RIGHT);
            break;
        }
        default:
            break;
    }
}


void Player::drillIntoSubstance(Substance* s, int direction){
    if (s == nullptr)
        return;
    double xCenterOfPlayer = collisionBox->x + (0.5 * collisionBox->w);
    double xCenterOfSubstance = s->collisionBox.x + (0.5 * s->collisionBox.w);
    //Only play 'gained item sound' if the substance is NOT dirt
    if(s->substanceType != "Dirt")
        playFoundSubstanceSound();
    
    switch(direction)
    {
        case DOWN:
        {
                if(yPos <= (s->collisionBox.y + s->collisionBox.h +5))
                {
                    yPos +=drillSpeed;
                    updateCollisionBox();
                }
                //If player isn't aligned with center.x of substance being drilled, move to center
                if (!isWithinRange(xCenterOfPlayer, xCenterOfSubstance-1, xCenterOfSubstance+1))
                {
                      if (xCenterOfPlayer > xCenterOfSubstance)
                      {
                          xPos -= 2;
                          updateCollisionBox();
                      }
                      else
                      {
                          xPos +=2;
                          updateCollisionBox();
                      }
                }
            break;
        }
        case LEFT:
        {
            if(isWithinRange(s->collisionBox.x, this->collisionBox->x - 5, this->collisionBox->x + 5))
            {
                xPos-= drillSpeed;
                updateCollisionBox();
            }
            break;
        }
        case RIGHT:
        {
            xPos+= drillSpeed;
            updateCollisionBox();
            break;
        }
    }
}

void Player::destroySubstance(){
    removeSubstanceFromMap();
}

Substance* Player::getSubstanceToLeft(){
    Substance* substanceLeft = nullptr;
    for(const auto& Substance : Map::substances)
    {
        if(isSubstanceLeftOfPlayer(Substance))
            substanceLeft = Substance;
    }
    return substanceLeft;
}

Substance* Player::getSubstanceToRight(){
    Substance* substanceRight = nullptr;
    for(const auto& Substance : Map::substances)
    {
        if(isSubstanceRightOfPlayer(Substance))
            substanceRight = Substance;
    }
    return substanceRight;
}


Substance* Player::getSubstanceDirectlyBelow(){
    list<Substance*> substancesTouching;
    for(const auto& Substance : Map::substances)
    {
        //is substance.y directly below player?
        if(isSubstanceBelowPlayer(Substance))
        {
            //is left bottom corner of vehicle in alignment with substance.x?
            if (isWithinRange(collisionBox->x, Substance->collisionBox.x, Substance->collisionBox.x + Substance->collisionBox.w))
                substancesTouching.push_back(Substance);
            
            //is right bottom corner of vehicle in alignment with substance.x?
            if (isWithinRange(collisionBox->x + collisionBox->w, Substance->collisionBox.x, Substance->collisionBox.x + Substance->collisionBox.w))
                substancesTouching.push_back(Substance);
        }
    }
    
    double xCenterOfPlayer = collisionBox->x + (0.5 * collisionBox->w);
    Substance* closestSubstance = nullptr;
    for(const auto& Substance : substancesTouching)
    {
        if(closestSubstance == nullptr)
        {
            closestSubstance = Substance;
            continue;
        }
        
        double xCenterOfSubstance = Substance->collisionBox.x + (0.5 * Substance->collisionBox.w);
        double xCenterOfClosestSubstance = closestSubstance->collisionBox.x + (0.5 * closestSubstance->collisionBox.w);
        
        if((abs(xCenterOfSubstance - xCenterOfPlayer)) < (abs(xCenterOfClosestSubstance - xCenterOfPlayer)))
        {
            closestSubstance = Substance;
        }
    }
    
    return closestSubstance;
}


void Player::changeAnimation(int newAnimation){
    
}

bool isWithinRange(double testNum, double lowNum, double highNum){
    if (testNum <= highNum && testNum >= lowNum)
        return true;
    return false;
}

bool Player::isSubstanceLeftOfPlayer(Substance* s)
{
    //Does substance.x aligns with player.x
    if(isWithinRange(s->collisionBox.x + s->collisionBox.w, this->collisionBox->x - 10, this->collisionBox->x+10))
    {
        //Does substance.y align with player.y
        if(isWithinRange((s->collisionBox.y + 0.5 * s->collisionBox.h), this->collisionBox->y-10, this->collisionBox->y + this->collisionBox->h+10))
        {
            return true;
        }
    }
    return false;
}


bool Player::isSubstanceRightOfPlayer(Substance* s){

    //Does substance.x align with player.x
    if (isWithinRange(s->collisionBox.x, this->collisionBox->x + (this->collisionBox->w - 10), this->collisionBox->x + (this->collisionBox->w + 10)))
    {
        //Does substance.y align with player.y
        if(isWithinRange((s->collisionBox.y + 0.5 * s->collisionBox.h), this->collisionBox->y-10, this->collisionBox->y + this->collisionBox->h+10))
        {
            return true;
        }
    }
    return false;
}

bool Player::isSubstanceAbovePlayer(Substance* s){
    //Returns true if substance is above player
    if(isWithinRange((s->collisionBox.y+s->collisionBox.h-4), this->collisionBox->y - 5, this->collisionBox->y+2))
    {
        return true;
    }
    
    return false;
}

bool Player::isSubstanceBelowPlayer(Substance* s){
    //returns true if substance is below player
    if(isWithinRange(s->collisionBox.y, this->collisionBox->y + this->collisionBox->h - 6, this->collisionBox->y + this->collisionBox->h + 6))
    {
        return true;
    }
    
    return false;
}


void Player::removeSubstanceFromMap(){
    //deletes substance from map array so it is not longer drawn
    Map::substances.remove(substanceBeingDrilled);
}

void Player::drawFuelTank(){
    
    //Black Outline of Tank:
    SDL_Rect *destRect = new SDL_Rect();
    destRect->x = 850;
    destRect->y = 5;
    destRect->w = 40;
    destRect->h = 100;
    SDL_SetRenderDrawColor(Game::renderer, 0,0,0, 0);
    SDL_RenderDrawRect(Game::renderer, destRect);
    SDL_RenderFillRect(Game::renderer, destRect);
    
    //Clear Sky Inner of Tank:
    destRect->x = 852;
    destRect->y = 7;
    destRect->w = 36;
    destRect->h = 96;
    SDL_SetRenderDrawColor(Game::renderer, 135,206,250, 0);
    SDL_RenderDrawRect(Game::renderer, destRect);
    SDL_RenderFillRect(Game::renderer, destRect);

    //Petrol Inside Tank:
    if(fuel > 5)
    {
        destRect->y = 107 - fuel;
        destRect->h = -2 + fuel;
        SDL_SetRenderDrawColor(Game::renderer, 124, 252, 0, 0);
        SDL_RenderDrawRect(Game::renderer, destRect);
        SDL_RenderFillRect(Game::renderer, destRect);
    }
    
}

void Player::drawPlayerHealth(){
    
    SDL_Rect *destRect = new SDL_Rect();
    destRect->x = 850;
    destRect->y = 150;
    destRect->w = 40;
    destRect->h = 100;
     SDL_SetRenderDrawColor(Game::renderer, 0,0,0, 0);
     SDL_RenderDrawRect(Game::renderer, destRect);
     SDL_RenderFillRect(Game::renderer, destRect);
    
    SDL_RenderSetScale(Game::renderer, 1.00, 1.00);
    //Clear Sky Inner of Tank:
    destRect->x = 852;
    destRect->y = 152;
    destRect->w = 36;
    destRect->h = 96;
    SDL_SetRenderDrawColor(Game::renderer, 135,206,250, 0);
    SDL_RenderDrawRect(Game::renderer, destRect);
    SDL_RenderFillRect(Game::renderer, destRect);
    
    //Player Health
    if(playerHealth > 2)
    {
        destRect->y = 252 - playerHealth;
        destRect->h = -2 + playerHealth;
        SDL_SetRenderDrawColor(Game::renderer, 247, 0, 0, 0);
        SDL_RenderDrawRect(Game::renderer, destRect);
        SDL_RenderFillRect(Game::renderer, destRect);
    }
    
    destRect->y = 248;
    destRect->x = 888;
    
    SDL_Rect heartDestRect = { 872, 242, 19, 19 };
    SDL_Rect whiteHeartDestRect = { 869, 240, 25, 25 };
    TextureManager::DrawStill(whiteHeart, sourceRect, whiteHeartDestRect);
    TextureManager::DrawStill(heart, sourceRect, heartDestRect);
    
}

void Player::damageFlash(){
    SDL_SetRenderDrawColor(Game::renderer, 255, 0, 0, 0);
    SDL_RenderClear(Game::renderer);
    SDL_Delay(100);
    
}

bool Player::checkCollisionWithMapBoundsLeft(){
    if(isWithinRange(xPos, -50, 7))
        return true;
    
    return false;
}

bool Player::checkCollisionWithMapBoundsRight(){
    if(xPos > Game::LEVEL_WIDTH - 50)
        return true;
    
    return false;
}

bool Player::checkIfNearAlien(){
    
    if(isWithinRange(xPos, 2070, 2250))
    {
        if(isWithinRange(yPos, 330, 400))
            return true;
    }
    return false;
}

bool Player::checkIfCanAffordEndGame(){
    
    if(money >= 1000000)
    {
        return true;
    }
    return false;
}

void Player::updatePlayerStats(){
    switch(drillSkillLevel)
    {
        case 1:
        {
            drillSpeed = 1;
            break;
        }
        case 2:
        {
            drillSpeed = 2;
            break;
        }
        case 3:
        {
            drillSpeed = 5;
            break;
        }
        case 4:
        {
            drillSpeed = 10;
            break;
        }
    }
    
    switch(engineSkillLevel)
    {
        case 1:
        {
            engineSpeed = 2;
            break;
        }
        case 2:
        {
            engineSpeed = 4;
            break;
        }
        case 3:
        {
            engineSpeed = 6;
            break;
        }
        case 4:
        {
            engineSpeed = 10;
            break;
        }
    }
    
    switch(fuelTankSkillLevel)
    {
        case 1:
        {
            fuelCost = 0.08;
            break;
        }
        case 2:
        {
            fuelCost = 0.04;
            break;
        }
        case 3:
        {
            fuelCost = 0.02;
            break;
        }
        case 4:
        {
            fuelCost = 0.01;
            break;
        }
    }
}

bool Player::checkForPlayerDeath(){
    if(playerHealth < 1 || fuel <1)
    {
        playerIsDead = true;
        return true;
    }

    return false;
}


void Player::playFoundSubstanceSound(){
        if ( Mix_PlayChannel(DINGCHANNEL, this->foundSubstanceSound, 0) == -1 )
            std::cerr << "Failed to play effect\n";
        else
            cout<<"success"<<endl;
}

