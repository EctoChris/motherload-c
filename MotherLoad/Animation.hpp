#ifndef Animation_hpp
#define Animation_hpp

#include <string>
#include <stdio.h>
#include "Game.hpp"


using namespace std;

class Animation {
    
public:
    //Basic information we need:
    int type;
    int numberOfFrames;
    int yStartPos;
    int xStartPos;
    float yPos;
    int frameWidth, frameHeight, speed;
    bool isHorizontal;
    SDL_Texture* spriteSheet;
    SDL_Rect sourceRect;
    float frameDuration; //which frame to show
    float frameTimer; //how long current frame has been shown on screen
    
    //Current state of animation:
    int currentFrame; //which frame to show
    
    //Methods
    Animation(const char* animationName, int numberOfFrames, int frameWidth, int frameHeight, float speed, int r, int g, int b, bool isHorizontal, int xStartPos, int yStartPos);
    Animation();
    ~Animation();
    void update(float dt);
    void runThroughAnimation(float dt);
    void draw(int x, int y);
    void drawStill(int x, int y);
    void draw(int x, int y, double scale);
    void draw(int x, int y, bool flip);
    void draw(int x, int y, double scale, bool flip);
    void draw(int x, int y, double scale, const double angle, SDL_Point *rotationPoint, bool flip);
    
};

#endif /* Animation_hpp */
