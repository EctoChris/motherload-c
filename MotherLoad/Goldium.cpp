#include "Goldium.hpp"

void Goldium::Draw(){
    TextureManager::Draw(texture, sourceRect, destRect);
}

void Goldium::ShowSuccessMessage(int xPos, int yPos, float DT){
    textDestination = { xPos, yPos, 100, 25 };
    SDL_Rect sourceRect = { 1, 1};
    
    textTexture = TextureManager::CreateTextTexture("+ 1 Goldium", Game::greenColor);
    TextureManager::Draw(textTexture, sourceRect, textDestination);
}

void Goldium::updateMessage(){
    if(textTexture)
    {
        TextureManager::Draw(textTexture, sourceRect, textDestination);
        textDestination.y-= 10;
    }
}
