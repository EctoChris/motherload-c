#include "TextureManager.hpp"

//Method for loading textures with a temporary surface
SDL_Texture* TextureManager::LoadTexture(const char* fileName)
{
    SDL_Surface* tempSurface = IMG_Load(fileName);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(Game::renderer, tempSurface);
    SDL_FreeSurface(tempSurface);
    
    return texture;
}

//Method for loading textures with a temporary surface + making the background transparent
SDL_Texture* TextureManager::LoadTextureAndMakeTransparent(const char* fileName, int r, int g, int b){
    SDL_Surface* tempSurface = IMG_Load(fileName);
    SDL_SetColorKey(tempSurface, 1, SDL_MapRGB(tempSurface->format, r, g, b));
    SDL_Texture* texture = SDL_CreateTextureFromSurface(Game::renderer, tempSurface);
    SDL_FreeSurface(tempSurface);

    return texture;
}

//Method for drawing a texture
void TextureManager::Draw(SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect)
{
    destRect.x = destRect.x - Game::camera.x;
    destRect.y = destRect.y - Game::camera.y;
    SDL_RenderCopy(Game::renderer, texture, NULL, &destRect);
}

//Drawing a texture with the ability for the texture to be drawn 'flipped'
void TextureManager::Draw(SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect, bool flip){
    destRect.x = destRect.x - Game::camera.x;
    destRect.y = destRect.y - Game::camera.y;
    SDL_RendererFlip flipType = SDL_FLIP_NONE;
    if (flip)
        flipType = SDL_FLIP_HORIZONTAL;
    
    SDL_RenderCopyEx(Game::renderer, texture, &sourceRect, &destRect, 0, NULL, flipType);
}

//Drawing a texture with the ability for the texture to be scaled to different sizes
void TextureManager::Draw(SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect, float scale){
    destRect.x = destRect.x - Game::camera.x;
    destRect.y = destRect.y - Game::camera.y;
    destRect.w = destRect.w * scale;
    destRect.h = destRect.h * scale;
}

SDL_Texture* TextureManager::CreateTextTexture(const char* message, SDL_Color colour){
    
    //Generate Surface from font + string:
    SDL_Surface* textSurface = TTF_RenderText_Blended(Game::caviarFont, message, colour);
    
    //Convert to Texture:
    SDL_Texture* textTexture = SDL_CreateTextureFromSurface(Game::renderer, textSurface);

    //Delete surface:
    SDL_FreeSurface(textSurface);
    
    
    return textTexture;
}

//Drawing a texture in one position (ignoring the camera's position)
void TextureManager::DrawStill(SDL_Texture* texture, SDL_Rect sourceRect, SDL_Rect destRect){
    SDL_RenderCopy(Game::renderer, texture, NULL, &destRect);
}
