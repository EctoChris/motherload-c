#include "HealthShop.hpp"


HealthShop::HealthShop(Player* p, Game* g){
    this->player = p;
    this->game = g;
    successMessage = TextureManager::LoadTexture("SuccessMessage.png");
}

HealthShop::~HealthShop(){
    SDL_DestroyTexture(successMessage);
}

//display either success / failure message for healing player
void HealthShop::generateSuccessMessage(bool healingWasPurchased){
    showingSuccessMessage = true;
    string recentHealthPointsHealedString = to_string(recentHealthPointsHealed);
    string money = to_string(healingCost);
    string totalMessage;
    if(healingWasPurchased)
    {
        string message = "You healed ";
        string message2 = " health points, costing $";
        totalMessage = message.append(recentHealthPointsHealedString).append(message2).append(money);
    }
    else
        totalMessage = "Sorry you cannot afford to heal at the moment.";
    
    //Drawing GUI Canvas:
    SDL_Rect destRect = { 200, 200, 500, 200 };
    SDL_Rect sourceRect = { 10, 10, 10, 10 };
    TextureManager::DrawStill(successMessage, sourceRect, destRect);
    
    //Drawing Text Message:
    destRect = { 230, 240, 400, 50 };
    const char* messageChar = totalMessage.c_str();
    TextureManager::DrawStill(TextureManager::CreateTextTexture(messageChar, Game::greenColor), sourceRect, destRect);

}

//Conditions for when player attempts to heal:
bool HealthShop::healPlayer(){
    if(player->playerHealth == 100)
        return false;
    
    healthPointsToHeal = 100 - player->playerHealth;
    healingCost = healthPointsToHeal * 5;
    
    if(player->money >= healingCost)
    {
        recentHealthPointsHealed = healthPointsToHeal;
        player->playerHealth = 100;
        player->money -= healingCost;
        generateSuccessMessage(true);
        lastHealWasSuccess = true;
        return true;
    }
    else if(player->money > 0)
    {
        recentHealthPointsHealed = player->money / 5;
        healingCost = player->money;
        player->money -= healingCost;
        player->playerHealth += recentHealthPointsHealed;
        generateSuccessMessage(true);
        lastHealWasSuccess = true;
        return true;
    }
    else{
        generateSuccessMessage(false);
        lastHealWasSuccess = false;
        return true;
    }
}
