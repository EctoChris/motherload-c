#ifndef MineralProcessor_hpp
#define MineralProcessor_hpp

#include <stdio.h>
#include "Player.hpp"
#include <string>

using namespace std;

class MineralProcessor {
public:
    Player* player;
    Game* game;
    SDL_Texture* mainScreen = nullptr;
    SDL_Texture* successMessage = nullptr;
    int totalSaleValue = 0;
    int lastSale;
    bool stockToSell = false;
    bool showingSuccessMessage = false;
    MineralProcessor(Player* player, Game* game);
    ~MineralProcessor();
    void generateShopScreen();
    void showSuccessMessage();
    void handleInputs();
    void sellAll();
    
};

#endif /* MineralProcessor_hpp */
