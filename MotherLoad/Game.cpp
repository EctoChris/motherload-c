#include "Game.hpp"

enum screenSelection {
    titleScreen = 0,
    gameScreen = 1,
    mineralProcessorScreen = 2,
    fuelStationScreen = 3,
    upgradeShopScreen = 4,
    alienSpeechFail = 5,
    alienSpeechSuccess = 6,
    endGameAnimation = 7,
    healthStationMessage = 8,
    instructionsScreen = 9
};


SDL_Renderer* Game::renderer = nullptr;
SDL_Event Game::event;
TTF_Font* Game::caviarFont = nullptr;
SDL_Color Game::greenColor;
SDL_Color Game::blackColor;
SDL_Color Game::redColor;
SDL_Rect Game::camera;
int Game::screenWidth  = 900;
int Game::screenHeight = 640;
int Game::LEVEL_WIDTH = 2500;
int Game::LEVEL_HEIGHT = 960;
int Game::currentScreen = titleScreen;

Game::Game()
{
    
}

Game::~Game()
{
    
}

void Game::initText(){
    caviarFont = TTF_OpenFont("Caviar_Dreams_Bold.ttf", 16); //font location, font size
    greenColor = {124, 252, 0, 0};
    blackColor = { 0, 0, 0, 0 };
}


void Game::init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen)
{
    int flags = SDL_WINDOW_SHOWN;
    if(fullscreen)
        flags = SDL_WINDOW_FULLSCREEN;
    
    //Initialize SDL2 and subsystems
    if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        window = SDL_CreateWindow("MotherLoad", xpos, ypos, width, height, flags);
        renderer = SDL_CreateRenderer(window, -1, 0);
        
        if(renderer)
            SDL_SetRenderDrawColor(renderer, 255, 0, 15, 255);
        
        isRunning = true;
    }
    else {
        isRunning = false;
    }
    
   
    if( !(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
    {
        cout<<"Could not initialize SDL_Image library"<<endl;
    }
    
    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
    {
        cout<<"Mixer could not load"<<endl;
        SDL_Quit();
    }
    else {
        cout<<"Yeww mixer loaded"<<endl;
    }
}

void Game::update()
{
    
}

//This is where we add all our textures to be rendered. Each new texture rendered will be on top of previous (render background 1st, then add player)
void Game::render()
{
    SDL_RenderClear(renderer);
  //  player->Render();
    SDL_RenderPresent(renderer);
}

void Game::clean()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

}

void Game::endGameAnimation()
{
    
}
