#ifndef RareItem_hpp
#define RareItem_hpp

#include <stdio.h>
#include "Game.hpp"
#include "VeryRareItem.hpp"

using namespace std;

class RareItem: public VeryRareItem {
public:
    RareItem(int type) : VeryRareItem(type){
        
    }
};
#endif /* RareItem_hpp */
